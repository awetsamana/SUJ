SELECT msi.segment1 item, msi.inventory_item_id,hou.NAME ORGANIZATION, mp.organization_code, min_minmax_quantity min_qty, max_minmax_quantity max_qty,
msi.CREATION_DATE, msi.LAST_UPDATE_DATE
  FROM mtl_system_items msi, hr_all_organization_units hou, mtl_parameters mp
  WHERE hou.organization_id = msi.organization_id
  and hou.organization_id = mp.organization_id
   AND (min_minmax_quantity IS NOT NULL OR max_minmax_quantity IS NOT NULL) 
    order by 2, 1

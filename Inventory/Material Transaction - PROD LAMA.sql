/* Formatted on 12/18/2017 11:26:45 AM (QP5 v5.256.13226.35510) */
WITH cogm_category
     AS (  SELECT mic.inventory_item_id, mic.organization_id, MAX ( mc.attribute1) cogm_category
             FROM mtl_item_categories mic, mtl_categories mc, mtl_category_sets mics
            WHERE     mic.category_id = mc.category_id
                  AND mc.STRUCTURE_ID = mics.STRUCTURE_ID
                  AND mics.category_set_name = 'SUJ_COGM_Category'
                  AND mic.category_set_id = mics.category_set_id
         GROUP BY mic.inventory_item_id, mic.organization_id)
   , inv_category
     AS (  SELECT mic.inventory_item_id, mic.organization_id, MAX ( mc.attribute1) inv_category
             FROM mtl_item_categories mic, mtl_categories mc, mtl_category_sets mics
            WHERE     mic.category_id = mc.category_id
                  AND mc.STRUCTURE_ID = mics.STRUCTURE_ID
                  AND mics.category_set_name = 'SUJ_Inventory_Category'
                  AND mic.category_set_id = mics.category_set_id
         GROUP BY mic.inventory_item_id, mic.organization_id)
SELECT mmt.transaction_id
     , mmt.transaction_source_name
     , mp.organization_code
     , msi.segment1
     , mtt.transaction_type_name
     , flv.meaning transaction_action
     , mtxs.transaction_source_type_name
     , mmt.transaction_source_id
     , mmt.transaction_quantity
     , mmt.primary_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , CASE
          WHEN FLV.MEANING = 'Cost update'
          THEN
             (SELECT base_transaction_value
                FROM mtl_transaction_accounts
               WHERE transaction_id = mmt.transaction_id AND ACCOUNTING_LINE_TYPE = 1)
          ELSE
             CASE WHEN uc.amount IS NOT NULL THEN uc.amount * SIGN ( mmt.transaction_quantity) ELSE mmt.actual_cost * mmt.transaction_quantity END
       END
          amount
     --, CASE WHEN uc.amount IS NOT NULL THEN uc.amount * SIGN ( mmt.transaction_quantity) ELSE mmt.actual_cost * mmt.transaction_quantity END amount
     , mmt.subinventory_code
     , mil.concatenated_segments locator
     , mmt.attribute_category
     , mmt.attribute1
     , mmt.attribute2
     , mmt.attribute3
     , mmt.attribute4
     , mmt.attribute5
     , mmt.attribute6
     , mmt.attribute7
     , mmt.attribute8
     , mmt.attribute9
     , mmt.attribute10
     , mmt.attribute11
     , mmt.attribute12
     , mmt.attribute13
     , mmt.attribute14
     , mmt.attribute15
     , mmt.shipment_number
     , mp2.organization_code transfer_organization_code
     , mmt.creation_date
     , mmt.last_update_date
     , DECODE (mmt.logical_transaction, 1, 'Y', 'N') Logical_transactions
     , cogmc.cogm_category
     , invc.inv_category
     , NVL (invc.inv_category, cogmc.cogm_category) item_type
  FROM mtl_material_transactions mmt
     , mtl_parameters mp
     , mtl_parameters mp2
     , mtl_transaction_types mtt
     , mtl_system_items_kfv msi
     , mtl_item_locations_kfv mil
     , fnd_lookup_values flv
     , mtl_txn_source_types mtxs
     , (  SELECT cd.transaction_id, ABS ( SUM ( cd.layer_quantity * NVL (slc.layer_cost, cd.actual_cost))) amount
            FROM mtl_cst_layer_act_cost_details cd, suj_fg_layer_cost slc
           WHERE cd.inv_layer_id = slc.inv_layer_id(+)
        GROUP BY transaction_id) uc
     , cogm_category cogmc
     , inv_category invc
 WHERE     mmt.organization_id = mp.organization_id
       AND mmt.transfer_organization_id = mp2.organization_id(+)
       AND mtt.transaction_type_id = mmt.transaction_type_id
       AND mmt.organization_id = msi.organization_id
       AND msi.inventory_item_id = mmt.inventory_item_id
       AND mil.inventory_location_id(+) = mmt.locator_id
       AND flv.lookup_type = 'MTL_TRANSACTION_ACTION'
       AND flv.lookup_code = mtt.transaction_action_id
       AND mtxs.transaction_source_type_id = mmt.transaction_source_type_id
       AND msi.concatenated_segments LIKE '1-FG%'
       AND CASE WHEN mmt.transaction_action_id IN (2, 28) AND SIGN ( mmt.transaction_quantity) = 1 THEN mmt.transfer_transaction_id ELSE mmt.transaction_Id END =
              uc.transaction_id(+)
       AND mmt.inventory_item_id = cogmc.inventory_item_id(+)
       AND mmt.organization_id = cogmc.organization_id(+)
       AND mmt.inventory_item_id = invc.inventory_item_id(+)
       AND mmt.organization_id = invc.organization_id(+)
UNION ALL
--Prod Lama dan non-FG
SELECT mmt.transaction_id
     , mmt.transaction_source_name
     , mp.organization_code
     , msi.segment1
     , mtt.transaction_type_name
     , flv.meaning transaction_action
     , mtxs.transaction_source_type_name
     , mmt.transaction_source_id
     , mmt.transaction_quantity
     , mmt.primary_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , CASE
          WHEN FLV.MEANING = 'Cost update'
          THEN
             (SELECT base_transaction_value
                FROM mtl_transaction_accounts
               WHERE transaction_id = mmt.transaction_id AND ACCOUNTING_LINE_TYPE = 1)
          ELSE
             CASE WHEN uc.amount IS NOT NULL THEN uc.amount * SIGN ( mmt.transaction_quantity) ELSE mmt.actual_cost * mmt.transaction_quantity END
       END
          amount
     --, CASE WHEN uc.amount IS NOT NULL THEN uc.amount * SIGN ( mmt.transaction_quantity) ELSE mmt.actual_cost * mmt.transaction_quantity END amount
     , mmt.subinventory_code
     , mil.concatenated_segments locator
     , mmt.attribute_category
     , mmt.attribute1
     , mmt.attribute2
     , mmt.attribute3
     , mmt.attribute4
     , mmt.attribute5
     , mmt.attribute6
     , mmt.attribute7
     , mmt.attribute8
     , mmt.attribute9
     , mmt.attribute10
     , mmt.attribute11
     , mmt.attribute12
     , mmt.attribute13
     , mmt.attribute14
     , mmt.attribute15
     , mmt.shipment_number
     , mp2.organization_code transfer_organization_code
     , mmt.creation_date
     , mmt.last_update_date
     , DECODE (mmt.logical_transaction, 1, 'Y', 'N') Logical_transactions
     , cogmc.cogm_category
     , invc.inv_category
     , NVL (invc.inv_category, cogmc.cogm_category) item_type
  FROM mtl_material_transactions mmt
     , mtl_parameters mp
     , mtl_parameters mp2
     , mtl_transaction_types mtt
     , mtl_system_items_kfv msi
     , mtl_item_locations_kfv mil
     , fnd_lookup_values flv
     , mtl_txn_source_types mtxs
     , (  SELECT transaction_id, ABS ( SUM ( layer_quantity * actual_cost)) amount
            FROM mtl_cst_layer_act_cost_details
        GROUP BY transaction_id) uc
     , cogm_category cogmc
     , inv_category invc
 WHERE     mmt.organization_id = mp.organization_id
       AND mmt.transfer_organization_id = mp2.organization_id(+)
       AND mtt.transaction_type_id = mmt.transaction_type_id
       AND mmt.organization_id = msi.organization_id
       AND msi.inventory_item_id = mmt.inventory_item_id
       AND mil.inventory_location_id(+) = mmt.locator_id
       AND flv.lookup_type = 'MTL_TRANSACTION_ACTION'
       AND flv.lookup_code = mtt.transaction_action_id
       AND mtxs.transaction_source_type_id = mmt.transaction_source_type_id
       AND msi.concatenated_segments NOT LIKE '1-FG%'
       AND CASE WHEN mmt.transaction_action_id IN (2, 28) AND SIGN ( mmt.transaction_quantity) = 1 THEN mmt.transfer_transaction_id ELSE mmt.transaction_Id END =
              uc.transaction_id(+)
       AND mmt.inventory_item_id = cogmc.inventory_item_id(+)
       AND mmt.organization_id = cogmc.organization_id(+)
       AND mmt.inventory_item_id = invc.inventory_item_id(+)
       AND mmt.organization_id = invc.organization_id(+)
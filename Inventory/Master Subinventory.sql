/* Formatted on 11/27/2017 6:52:07 PM (QP5 v5.256.13226.35510) */
SELECT mp.organization_id
     , mp.organization_code
     , msi.secondary_inventory_name
     , msi.description
  FROM mtl_parameters mp, mtl_secondary_inventories msi
 WHERE msi.organization_id = mp.organization_id
 
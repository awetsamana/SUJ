/* Formatted on 11/28/2017 2:49:44 PM (QP5 v5.256.13226.35510) */
SELECT mvh.attribute1 mo_reference_number
     , mvh_ref.header_id mo_reference_id
     , mvh.request_number mo_retur_number
     , mvh.header_id mo_return_id
     , mvh.creation_date mo_retur_date
     , mp.organization_code
     , mvl.line_number mo_line_number
     , mvl.line_id mo_line_id
     , msib.concatenated_segments item_code
     , mmt.transaction_quantity
     , mmt.transaction_uom
     , mmt.subinventory_code source_subinventory
     , mmt.transfer_subinventory destination_subinventory
     , mmt.transaction_id
     , harga_so.currency_code
     , harga_so.rate_idr_usd
     , harga_so.rate_usd_idr
     , harga_so.price_idr
     , harga_so.price_usd
     , harga_so.price_idr * mmt.transaction_quantity amount_idr
     , harga_so.price_usd * mmt.transaction_quantity amount_usd
     , mmt.creation_date
     , mmt.last_update_date
     , mtr.REASON_NAME
  FROM mtl_material_transactions mmt
     , mtl_txn_request_lines mvl
     , mtl_txn_request_headers mvh
     , mtl_txn_request_headers mvh_ref
     , mtl_parameters mp
     , mtl_system_items_kfv msib
     , mtl_transaction_reasons mtr
     , (  SELECT mvl.header_id mo_reference_id
               , oeh.transactional_curr_code currency_code
               , MAX ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'IDR', 'USD') END)
                    rate_idr_usd
               , MAX ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END)
                    rate_usd_idr
               , AVG ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END * oel.unit_selling_price)
                    price_idr
               , AVG ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE 1 END * oel.unit_selling_price) price_usd
            FROM mtl_material_transactions mmt
               , mtl_txn_request_lines mvl
               , oe_order_lines_all oel
               , oe_order_headers_all oeh
               , gl_daily_rates_v gdr
           WHERE     mmt.move_order_line_id = mvl.line_id
                 AND mmt.trx_source_line_id = oel.line_id
                 AND oel.header_id = oeh.header_id
                 AND mmt.subinventory_code = 'WHS_STG'
                 AND oeh.transactional_curr_code = gdr.from_currency(+)
                 AND gdr.from_currency(+) = 'IDR'
                 AND gdr.to_currency(+) = 'USD'
                 AND oeh.conversion_type_code = gdr.conversion_type(+)
                 AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
        GROUP BY mvl.header_id, oeh.transactional_curr_code) harga_so
 WHERE     mmt.transaction_source_type_id = 4
       AND mmt.transaction_action_id = 2
       AND mmt.subinventory_code = 'WHS_STG'
       AND mmt.trx_source_line_id = mvl.line_id
       AND mvl.header_id = mvh.header_id
       AND mvh_ref.request_number = mvh.attribute1
       AND mvh.attribute_category = 'Move Order Retur'
       AND mp.organization_id = mmt.organization_id
       AND mmt.inventory_item_id = msib.inventory_item_id
       AND mmt.reason_id = mtr.reason_id(+)
       AND mmt.organization_id = msib.organization_id
       AND harga_so.mo_reference_id = mvh_ref.header_id;
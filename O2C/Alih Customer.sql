/* Formatted on 11/28/2017 2:22:23 PM (QP5 v5.256.13226.35510) */
-----------------------------------------
-- Query Alih Customer

SELECT mtrh.request_number mo_reference_number
     , mtrh.header_id mo_reference_id
     , mmt2.transaction_id
     , mmt2.transaction_date transaction_date_dialihkan
     , k.delivery_id delivery_id_pengalihan
     , mp.organization_code
     , k.confirm_date delivery_pengalihan_date
     , k.delivery_new_quantity transaction_quantity
     , mmt2.transaction_uom
     , harga_so.currency_code
     , harga_so.rate_idr_usd
     , harga_so.rate_usd_idr
     , harga_so.price_idr
     , harga_so.price_usd
     , harga_so.price_idr * k.delivery_new_quantity amount_idr
     , harga_so.price_usd * k.delivery_new_quantity amount_usd
  FROM mtl_txn_request_lines mtrl
     , mtl_parameters mp
     , mtl_txn_request_headers mtrh
     , mtl_material_transactions mmt2
     , (  SELECT wnd.attribute8
               , SUM ( wdd.picked_quantity) delivery_new_quantity
               , wnd.delivery_id
               , wnd.confirm_date
            FROM wsh_delivery_details wdd, wsh_delivery_assignments wda, wsh_new_deliveries wnd
           WHERE wda.delivery_id = wnd.delivery_id(+) AND wdd.delivery_detail_id = wda.delivery_detail_id AND wnd.attribute8 IS NOT NULL
        GROUP BY wnd.attribute8, wnd.delivery_id, wnd.confirm_date) k
     , (  SELECT mvl.header_id mo_reference_id
               , oeh.transactional_curr_code currency_code
               , MAX ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'IDR', 'USD') END)
                    rate_idr_usd
               , MAX ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END)
                    rate_usd_idr
               , AVG ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END * oel.unit_selling_price)
                    price_idr
               , AVG ( CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE 1 END * oel.unit_selling_price) price_usd
            FROM mtl_material_transactions mmt
               , mtl_txn_request_lines mvl
               , oe_order_lines_all oel
               , oe_order_headers_all oeh
               , gl_daily_rates_v gdr
           WHERE     mmt.move_order_line_id = mvl.line_id
                 AND mmt.trx_source_line_id = oel.line_id
                 AND oel.header_id = oeh.header_id
                 AND mmt.subinventory_code = 'WHS_STG'
                 AND oeh.transactional_curr_code = gdr.from_currency(+)
                 AND gdr.from_currency(+) = 'IDR'
                 AND gdr.to_currency(+) = 'USD'
                 AND oeh.conversion_type_code = gdr.conversion_type(+)
                 AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
        GROUP BY mvl.header_id, oeh.transactional_curr_code) harga_so
 WHERE     1 = 1
       AND mtrl.header_id = mtrh.header_id
       AND mp.organization_id = mmt2.organization_id
       AND mmt2.move_order_line_id = mtrl.line_id
       AND mtrh.request_number = k.attribute8
       AND SUBSTR ( mmt2.subinventory_code, 1, 10) <> 'WHS_STG'
       AND harga_so.mo_reference_id = mtrh.header_id
--DELIVERY BARU DIMANA BARANG DIALIHKAN ME-REFER KE MO LAMA, QUANTITY YANG DIALIHKAN DIAMBIL QUANTITY DELIVERY BARU -NYA
--JIKA DELIVERY BARU SAMA DENGAN MO LAMA MAKA SEBESAR QUANTITY BARU DIANGGAP DIALIHKAN, JIKA DELIVERY BARU LEBIH KECIL DARI MO LAMA MAKA SEBESAR QUANTITY BARU DIANGGAP PENGALIHAN,
--JIKA DELIVERY BARU LEBIH BESAR DARI MO LAMA, MAKA SEBESAR SELISIH NYA DIANGGAP PENGALIHAN
/* Formatted on 12/12/2017 1:53:41 PM (QP5 v5.256.13226.35510) */
----Query Sales Order 3.1
--SELECT oeh.order_number
--     , oel.request_date
--     , oeh.creation_date
--     , oeh.ordered_date
--     , oeh.header_id
--     , oel.blanket_number sales_aggreement_number
--     , oeh.cust_po_number
--     , prl.name price_list_name
--     , oeh.price_list_id
--     , oot.name order_type
--     , oob.order_category_code
--     , flv.meaning order_status
--     , flv_line.meaning line_status
--     , oeh.transactional_curr_code currency_code
--     , 1 kurs_customer
--     , NULL conversion_type
--     , oeh.context header_context
--     , oeh.attribute1 header_attribute1
--     , oeh.attribute2 header_attribute2
--     , oeh.attribute3 header_attribute3
--     , oeh.attribute4 header_attribute4
--     , oeh.attribute5 header_attribute5
--     , oeh.attribute6 header_attribute6
--     , oeh.attribute7 header_attribute7
--     , oeh.attribute8 header_attribute8
--     , oeh.attribute9 header_attribute9
--     , oeh.attribute10 header_attribute10
--     , oeh.attribute11 header_attribute11
--     , oeh.attribute12 header_attribute12
--     , oeh.attribute13 header_attribute13
--     , oeh.attribute14 header_attribute14
--     , oeh.attribute15 header_attribute15
--     , oel.context line_context
--     , oel.attribute1 line_attribute1
--     , oel.attribute2 line_attribute2
--     , oel.attribute3 line_attribute3
--     , oel.attribute4 line_attribute4
--     , oel.attribute5 line_attribute5
--     , oel.attribute6 line_attribute6
--     , oel.attribute7 line_attribute7
--     , oel.attribute8 line_attribute8
--     , oel.attribute9 line_attribute9
--     , oel.attribute10 line_attribute10
--     , oel.attribute11 line_attribute11
--     , oel.attribute12 line_attribute12
--     , oel.attribute13 line_attribute13
--     , oel.attribute14 line_attribute14
--     , oel.attribute15 line_attribute15
--     -- , party.party_name customer
--     -- , bill_su.location customer_bill_to
--     -- , ship_su.location customer_ship_to
--     , oeh.sold_to_org_id customer_id
--     , oeh.invoice_to_org_id bill_to_location_id
--     , oeh.ship_to_org_id ship_to_location_id
--     , oel.line_number
--     , oel.line_id
--     , oel.ordered_item
--     , oel.ordered_quantity
--     , oel.unit_selling_price unit_price
--     , oel.unit_selling_price * oel.ordered_quantity amount
--     , oel.order_quantity_uom uom
--     , oeh.orig_sys_document_ref
--     , oos.name order_source
--     , oeh.creation_date header_creation_date
--     , oeh.last_update_date header_last_update_date
--     , oel.creation_date line_creation_date
--     , oel.last_update_date line_last_update_date
--  FROM oe_order_headers_all oeh
--     , oe_order_lines_all oel
--     , gl_sets_of_books sob
--     -- , hz_parties party
--     -- , hz_cust_accounts cust_acct
--     -- , hz_cust_site_uses_all bill_su
--     -- , hz_cust_site_uses_all ship_su
--     , qp_secu_list_headers_v prl
--     , oe_transaction_types_tl oot
--     , oe_transaction_types_all oob
--     , fnd_lookup_values_vl flv
--     , fnd_lookup_values_vl flv_line
--     , oe_order_sources oos
-- WHERE     oeh.header_id = oel.header_id
--       AND sob.set_of_books_id = fnd_profile.VALUE ( 'GL_SET_OF_BKS_ID')
--       AND oeh.transactional_curr_code = sob.currency_code
--       --AND oeh.sold_to_org_id = cust_acct.cust_account_id(+)
--       --AND cust_acct.party_id = party.party_id(+)
--       --AND oeh.invoice_to_org_id = bill_su.site_use_id(+)
--       --AND oeh.ship_to_org_id = ship_su.site_use_id(+)
--       AND oeh.price_list_id = prl.list_header_id(+)
--       AND oot.transaction_type_id = oeh.order_type_id
--       AND oob.transaction_type_id = oeh.order_type_id
--       AND oot.language = USERENV ( 'LANG')
--       AND flv.lookup_type = 'IBE_ORDER_STATUS_LOOKUP'
--       AND flv.lookup_code = oeh.flow_status_code
--       AND flv_line.lookup_type = 'LINE_FLOW_STATUS'
--       AND flv_line.lookup_code = oel.flow_status_code
--       AND oos.order_source_id = oeh.order_source_id
--UNION ALL
--SELECT oeh.order_number
--     , oel.request_date
--     , oeh.creation_date
--     , oeh.ordered_date
--     , oeh.header_id
--     , oel.blanket_number sales_aggreement_number
--     , oeh.cust_po_number
--     , prl.name price_list_name
--     , oeh.price_list_id
--     , oot.name order_type
--     , oob.order_category_code
--     , flv.meaning order_status
--     , flv_line.meaning line_status
--     , oeh.transactional_curr_code currency_code
--     , NVL (oeh.conversion_rate, gdr.conversion_rate) kurs_customer
--     , ct.user_conversion_type
--     , oeh.context header_context
--     , oeh.attribute1 header_attribute1
--     , oeh.attribute2 header_attribute2
--     , oeh.attribute3 header_attribute3
--     , oeh.attribute4 header_attribute4
--     , oeh.attribute5 header_attribute5
--     , oeh.attribute6 header_attribute6
--     , oeh.attribute7 header_attribute7
--     , oeh.attribute8 header_attribute8
--     , oeh.attribute9 header_attribute9
--     , oeh.attribute10 header_attribute10
--     , oeh.attribute11 header_attribute11
--     , oeh.attribute12 header_attribute12
--     , oeh.attribute13 header_attribute13
--     , oeh.attribute14 header_attribute14
--     , oeh.attribute15 header_attribute15
--     , oel.context line_context
--     , oel.attribute1 line_attribute1
--     , oel.attribute2 line_attribute2
--     , oel.attribute3 line_attribute3
--     , oel.attribute4 line_attribute4
--     , oel.attribute5 line_attribute5
--     , oel.attribute6 line_attribute6
--     , oel.attribute7 line_attribute7
--     , oel.attribute8 line_attribute8
--     , oel.attribute9 line_attribute9
--     , oel.attribute10 line_attribute10
--     , oel.attribute11 line_attribute11
--     , oel.attribute12 line_attribute12
--     , oel.attribute13 line_attribute13
--     , oel.attribute14 line_attribute14
--     , oel.attribute15 line_attribute15
--     -- , party.party_name customer
--     -- , bill_su.location customer_bill_to
--     -- , ship_su.location customer_ship_to
--     , oeh.sold_to_org_id customer_id
--     , oeh.invoice_to_org_id bill_to_location_id
--     , oeh.ship_to_org_id ship_to_location_id
--     , oel.line_number
--     , oel.line_id
--     , oel.ordered_item
--     , oel.ordered_quantity
--     , oel.unit_selling_price unit_price
--     , oel.unit_selling_price * oel.ordered_quantity amount
--     , oel.order_quantity_uom uom
--     , oeh.orig_sys_document_ref
--     , oos.name order_source
--     , oeh.creation_date header_creation_date
--     , oeh.last_update_date header_last_update_date
--     , oel.creation_date line_creation_date
--     , oel.last_update_date line_last_update_date
--  FROM oe_order_headers_all oeh
--     , oe_order_lines_all oel
--     , gl_sets_of_books sob2
--     -- , hz_parties party
--     -- , hz_cust_accounts cust_acct
--     -- , hz_cust_site_uses_all bill_su
--     -- , hz_cust_site_uses_all ship_su
--     , (SELECT gdr.conversion_type
--             , gdr.conversion_rate
--             , gdr.conversion_date
--             , gdr.from_currency
--             , gdr.to_currency
--          FROM gl_daily_rates gdr, gl_sets_of_books sob
--         WHERE sob.currency_code = gdr.to_currency AND sob.set_of_books_id = fnd_profile.VALUE ( 'GL_SET_OF_BKS_ID')) gdr
--     , qp_secu_list_headers_v prl
--     , oe_transaction_types_tl oot
--     , oe_transaction_types_all oob
--     , fnd_lookup_values_vl flv
--     , fnd_lookup_values_vl flv_line
--     , gl_daily_conversion_types ct
--     , oe_order_sources oos
-- WHERE     oeh.header_id = oel.header_id
--       AND sob2.set_of_books_id = fnd_profile.VALUE ( 'GL_SET_OF_BKS_ID')
--       AND oeh.transactional_curr_code <> sob2.currency_code
--       AND oeh.transactional_curr_code = gdr.from_currency(+)
--       AND oeh.conversion_type_code = gdr.conversion_type(+)
--       AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
--       --AND oeh.sold_to_org_id = cust_acct.cust_account_id(+)
--       --AND cust_acct.party_id = party.party_id(+)
--       --AND oeh.invoice_to_org_id = bill_su.site_use_id(+)
--       --AND oeh.ship_to_org_id = ship_su.site_use_id(+)
--       AND oeh.price_list_id = prl.list_header_id(+)
--       AND oot.transaction_type_id = oeh.order_type_id
--       AND oob.transaction_type_id = oeh.order_type_id
--       AND oot.language = USERENV ( 'LANG')
--       AND flv.lookup_type(+) = 'IBE_ORDER_STATUS_LOOKUP'
--       AND flv.lookup_code(+) = oeh.flow_status_code
--       AND flv_line.lookup_type(+) = 'LINE_FLOW_STATUS'
--       AND flv_line.lookup_code(+) = oel.flow_status_code
--       AND oeh.conversion_type_code = ct.conversion_type(+)
--       AND oos.order_source_id = oeh.order_source_id;
SELECT oeh.order_number
     , oel.request_date
     , oeh.creation_date
     , oeh.ordered_date
     , oeh.header_id
     , NVL (oel.blanket_number, oeh.blanket_number) sales_agreement_number
     , NVL (oobha.sales_document_name, oobha2.sales_document_name) sales_agreement_name
     , oeh.cust_po_number
     , prl.name price_list_name
     , oeh.price_list_id
     , oot.name order_type
     , oob.order_category_code
     , flv.meaning order_status
     , flv_line.meaning line_status
     , oeh.transactional_curr_code currency_code
     , gdr.conversion_rate rate_idr_usd
     , gdr.inverse_conversion_rate rate_usd_idr
     , ct.user_conversion_type
     , oeh.context header_context
     , oeh.attribute1 header_attribute1
     , oeh.attribute2 header_attribute2
     , oeh.attribute3 header_attribute3
     , oeh.attribute4 header_attribute4
     , oeh.attribute5 header_attribute5
     , oeh.attribute6 header_attribute6
     , oeh.attribute7 header_attribute7
     , oeh.attribute8 header_attribute8
     , oeh.attribute9 header_attribute9
     , oeh.attribute10 header_attribute10
     , oeh.attribute11 header_attribute11
     , oeh.attribute12 header_attribute12
     , oeh.attribute13 header_attribute13
     , oeh.attribute14 header_attribute14
     , oeh.attribute15 header_attribute15
     , oel.context line_context
     , oel.attribute1 line_attribute1
     , oel.attribute2 line_attribute2
     , oel.attribute3 line_attribute3
     , oel.attribute4 line_attribute4
     , oel.attribute5 line_attribute5
     , oel.attribute6 line_attribute6
     , oel.attribute7 line_attribute7
     , oel.attribute8 line_attribute8
     , oel.attribute9 line_attribute9
     , oel.attribute10 line_attribute10
     , oel.attribute11 line_attribute11
     , oel.attribute12 line_attribute12
     , oel.attribute13 line_attribute13
     , oel.attribute14 line_attribute14
     , oel.attribute15 line_attribute15
     , oeh.sold_to_org_id customer_id
     , oeh.invoice_to_org_id bill_to_location_id
     , oeh.ship_to_org_id ship_to_location_id
     , oel.line_number
     , oel.line_id
     , oel.ordered_item
     , oel.ordered_quantity
     , oel.unit_selling_price unit_price
     , oel.unit_selling_price * oel.ordered_quantity amount_idr
     , oel.unit_selling_price * oel.ordered_quantity * gdr.conversion_rate amount_usd
     , oel.order_quantity_uom uom
     , oeh.orig_sys_document_ref
     , oos.name order_source
     , oeh.creation_date header_creation_date
     , oeh.last_update_date header_last_update_date
     , oel.creation_date line_creation_date
     , oel.last_update_date line_last_update_date
  FROM oe_order_headers_all oeh
     , oe_order_lines_all oel
     , gl_sets_of_books sob2
     , (SELECT gdr.conversion_type
             , gdr.conversion_rate
             , gdr.conversion_date
             , gdr.from_currency
             , gdr.to_currency
             , gdr.inverse_conversion_rate
          FROM gl_daily_rates_v gdr, gl_sets_of_books sob
         WHERE     sob.currency_code = gdr.to_currency
               AND sob.set_of_books_id = (SELECT profile_option_value
                                            FROM fnd_profile_option_values pv, fnd_profile_options po
                                           WHERE po.profile_option_id = pv.profile_option_id AND po.profile_option_name = 'GL_SET_OF_BKS_ID' AND pv.level_id = 10001)) gdr
     , qp_secu_list_headers_v prl
     , oe_transaction_types_tl oot
     , oe_transaction_types_all oob
     , fnd_lookup_values_vl flv
     , fnd_lookup_values_vl flv_line
     , gl_daily_conversion_types ct
     , oe_order_sources oos
     , oe_blanket_headers_all oobha
     , oe_blanket_headers_all oobha2
 WHERE     oeh.header_id = oel.header_id
       AND sob2.set_of_books_id = (SELECT profile_option_value
                                     FROM fnd_profile_option_values pv, fnd_profile_options po
                                    WHERE po.profile_option_id = pv.profile_option_id AND po.profile_option_name = 'GL_SET_OF_BKS_ID' AND pv.level_id = 10001)
       AND oeh.transactional_curr_code <> sob2.currency_code
       AND oeh.transactional_curr_code = gdr.from_currency(+)
       AND oeh.conversion_type_code = gdr.conversion_type(+)
       AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
       AND oeh.price_list_id = prl.list_header_id(+)
       AND oot.transaction_type_id = oeh.order_type_id
       AND oob.transaction_type_id = oeh.order_type_id
       AND oot.language = USERENV ( 'LANG')
       AND flv.lookup_type(+) = 'IBE_ORDER_STATUS_LOOKUP'
       AND flv.lookup_code(+) = oeh.flow_status_code
       AND flv_line.lookup_type(+) = 'LINE_FLOW_STATUS'
       AND flv_line.lookup_code(+) = oel.flow_status_code
       AND oeh.conversion_type_code = ct.conversion_type(+)
       AND oos.order_source_id = oeh.order_source_id
       AND oel.blanket_number = oobha.order_number(+)
       AND oeh.blanket_number = oobha2.order_number(+)
UNION ALL
SELECT oeh.order_number
     , oel.request_date
     , oeh.creation_date
     , oeh.ordered_date
     , oeh.header_id
     , NVL (oel.blanket_number, oeh.blanket_number) sales_agreement_number
     , NVL (oobha.sales_document_name, oobha2.sales_document_name) sales_agreement_name
     , oeh.cust_po_number
     , prl.name price_list_name
     , oeh.price_list_id
     , oot.name order_type
     , oob.order_category_code
     , flv.meaning order_status
     , flv_line.meaning line_status
     , oeh.transactional_curr_code currency_code
     , suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'IDR', 'USD') rate_idr_usd
     , suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') rate_usd_idr
     , NULL conversion_type
     , oeh.context header_context
     , oeh.attribute1 header_attribute1
     , oeh.attribute2 header_attribute2
     , oeh.attribute3 header_attribute3
     , oeh.attribute4 header_attribute4
     , oeh.attribute5 header_attribute5
     , oeh.attribute6 header_attribute6
     , oeh.attribute7 header_attribute7
     , oeh.attribute8 header_attribute8
     , oeh.attribute9 header_attribute9
     , oeh.attribute10 header_attribute10
     , oeh.attribute11 header_attribute11
     , oeh.attribute12 header_attribute12
     , oeh.attribute13 header_attribute13
     , oeh.attribute14 header_attribute14
     , oeh.attribute15 header_attribute15
     , oel.context line_context
     , oel.attribute1 line_attribute1
     , oel.attribute2 line_attribute2
     , oel.attribute3 line_attribute3
     , oel.attribute4 line_attribute4
     , oel.attribute5 line_attribute5
     , oel.attribute6 line_attribute6
     , oel.attribute7 line_attribute7
     , oel.attribute8 line_attribute8
     , oel.attribute9 line_attribute9
     , oel.attribute10 line_attribute10
     , oel.attribute11 line_attribute11
     , oel.attribute12 line_attribute12
     , oel.attribute13 line_attribute13
     , oel.attribute14 line_attribute14
     , oel.attribute15 line_attribute15
     , oeh.sold_to_org_id customer_id
     , oeh.invoice_to_org_id bill_to_location_id
     , oeh.ship_to_org_id ship_to_location_id
     , oel.line_number
     , oel.line_id
     , oel.ordered_item
     , oel.ordered_quantity
     , oel.unit_selling_price unit_price
     , oel.unit_selling_price * oel.ordered_quantity * suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') amount_idr
     , oel.unit_selling_price * oel.ordered_quantity amount_usd
     , oel.order_quantity_uom uom
     , oeh.orig_sys_document_ref
     , oos.name order_source
     , oeh.creation_date header_creation_date
     , oeh.last_update_date header_last_update_date
     , oel.creation_date line_creation_date
     , oel.last_update_date line_last_update_date
  FROM oe_order_headers_all oeh
     , oe_order_lines_all oel
     , gl_sets_of_books sob
     , qp_secu_list_headers_v prl
     , oe_transaction_types_tl oot
     , oe_transaction_types_all oob
     , fnd_lookup_values_vl flv
     , fnd_lookup_values_vl flv_line
     , oe_order_sources oos
     , oe_blanket_headers_all oobha
     , oe_blanket_headers_all oobha2
 WHERE     oeh.header_id = oel.header_id
       AND sob.set_of_books_id = (SELECT profile_option_value
                                    FROM fnd_profile_option_values pv, fnd_profile_options po
                                   WHERE po.profile_option_id = pv.profile_option_id AND po.profile_option_name = 'GL_SET_OF_BKS_ID' AND pv.level_id = 10001)
       AND oeh.transactional_curr_code = sob.currency_code
       AND oeh.price_list_id = prl.list_header_id(+)
       AND oot.transaction_type_id = oeh.order_type_id
       AND oob.transaction_type_id = oeh.order_type_id
       AND oot.language = USERENV ( 'LANG')
       AND flv.lookup_type = 'IBE_ORDER_STATUS_LOOKUP'
       AND flv.lookup_code = oeh.flow_status_code
       AND flv_line.lookup_type = 'LINE_FLOW_STATUS'
       AND flv_line.lookup_code = oel.flow_status_code
       AND oos.order_source_id = oeh.order_source_id
       AND oel.blanket_number = oobha.order_number(+)
       AND oeh.blanket_number = oobha2.order_number(+)
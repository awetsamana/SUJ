SELECT mvh.request_number mo_reference_number
     , mvh.header_id mo_header_id
     , mmt.transaction_source_name write_off_source_number
     , mmt.transaction_date write_off_date
     , mp.organization_code
     , mmt.transaction_id
     , msib.concatenated_segments item_code
     , mmt.transaction_quantity
     , mmt.transaction_uom
     , mmt.subinventory_code source_subinventory
     , mmt.creation_date
     , mmt.last_update_date
  FROM mtl_material_transactions mmt
     , mtl_transaction_types mtt
     , mtl_parameters mp
     , mtl_txn_request_headers mvh
     , mtl_system_items_kfv msib
WHERE     mmt.transactioN_type_id = mtt.transaction_type_id
       AND mtt.transaction_type_name = '(-) Lost on Delivery'
       AND mp.organization_id = mmt.organization_id
       AND mvh.request_number(+) = mmt.attribute1
       AND mmt.inventory_item_id = msib.inventory_item_id
       AND mmt.organization_id = msib.organization_id;

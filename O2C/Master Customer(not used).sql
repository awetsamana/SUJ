 
  SELECT   
  cust.ACCOUNT_NUMBER customer_number, 
  cust.cust_account_id CUSTOMER_ACCOUNT_ID,
  party.party_name       CUSTOMER_NAME,
  CUST.STATUS,
  Site.LOCATION BILL_TO_LOCATION , acct.status SITE_STATUS,  site.SITE_USE_CODE, PARTY_SITE.ATTRIBUTE_CATEGORY, PARTY_SITE.ATTRIBUTE1, PARTY_SITE.ATTRIBUTE2,
  PARTY_SITE.ATTRIBUTE3,PARTY_SITE.ATTRIBUTE4,PARTY_SITE.ATTRIBUTE5,PARTY_SITE.ATTRIBUTE6,PARTY_SITE.ATTRIBUTE7,PARTY_SITE.ATTRIBUTE8,PARTY_SITE.ATTRIBUTE9,PARTY_SITE.ATTRIBUTE10,
  PARTY_SITE.ATTRIBUTE11,PARTY_SITE.ATTRIBUTE12,PARTY_SITE.ATTRIBUTE13,PARTY_SITE.ATTRIBUTE14,PARTY_SITE.ATTRIBUTE15,
  loc.ADDRESS1,loc.ADDRESS2,loc.ADDRESS3,loc.POSTAL_CODE ,loc.CITY 
          ,NVL(loc.STATE , loc.PROVINCE) PROVINCE ,DECODE(loc.COUNTRY,'ID','INDONESIA',loc.COUNTRY)  country,
          cust.CREATION_DATE, cust.LAST_UPDATE_DATE,
          site.CREATION_DATE site_creation_date, site.last_update_date site_last_update_date
 FROM   hz_cust_accounts cust, hz_cust_acct_sites_all acct, hz_cust_site_uses_all site, 
 hz_party_sites party_site ,hz_locations loc,hz_parties party 
 WHERE  cust.cust_account_id = acct.cust_account_id   
 AND    acct.cust_acct_site_id = site.cust_acct_site_id 
 and  site.SITE_USE_CODE = 'BILL_TO'
 AND    acct.ORG_ID = site.ORG_ID   
 and    loc.location_id = party_site.location_id 
 and     acct.party_site_id = party_site.party_site_id 
 and        cust.party_id = party.party_id 
 ORDER BY cust.ACCOUNT_NUMBER , SITE_USE_CODE
-- Master Customer 3.2
 SELECT
   cust.account_number customer_number
 , cust.cust_account_id customer_account_id
 , party.party_name customer_name
 , cust.status
   FROM
   hz_cust_accounts cust
 ,hz_parties party
  WHERE
   cust.party_id = party.party_id;
/* Formatted on 11/24/2017 5:10:10 PM (QP5 v5.256.13226.35510) */
---- versi 1
--SELECT oobha.sales_document_name sales_agreement_name
--     , oobha.order_number sales_agreement_number
--     , oobha.header_id sales_agreement_id
--     , oobha.creation_date sales_agreement_creation_date
--     , oobha.price_list_id
--     , prl.name price_list_name
--     , party.party_name customer
--     , cust_acct.account_number customer_number
--     , bill_su.location customer_bill_to
--     , ship_su.location customer_ship_to
--     , oobha.creation_date
--     , oobha.last_update_date
--  FROM oe_blanket_headers_all oobha
--     , qp_secu_list_headers_v prl
--     , hz_parties party
--     , hz_cust_accounts cust_acct
--     , hz_cust_site_uses_all bill_su
--     , hz_cust_site_uses_all ship_su
--WHERE     oobha.price_list_id = prl.list_header_id
--       AND oobha.sold_to_org_id = cust_acct.cust_account_id(+)
--       AND cust_acct.party_id = party.party_id(+)
--       AND oobha.invoice_to_org_id = bill_su.site_use_id(+)
--       AND oobha.ship_to_org_id = ship_su.site_use_id(+);
--
--      -- AND oobha.order_number =:P_SALES_AGREEMENT_NUMBER
--

--versi 20171116

SELECT oobha.sales_document_name sales_agreement_name
     , oobha.order_number sales_agreement_number
     , obeh.start_date_active activation_date
     , oobha.header_id sales_agreement_id
     , oobha.creation_date sales_agreement_creation_date
     , oobha.price_list_id
     , oobla.ordered_item sa_item
     , obet.blanket_min_quantity sa_quantity
     , oobla.order_quantity_uom sa_uom
     , prl.name price_list_name
     , party.party_name customer
     , cust_acct.account_number customer_number
     , bill_su.location customer_bill_to
     , ship_su.location customer_ship_to
     , prc_tbl.price
     , prc_tbl.currency_code
     , SUJ_GET_CLOSING_RATE ( obeh.start_date_active, 'USD', 'IDR') rate_usd_idr
     , SUJ_GET_CLOSING_RATE ( obeh.start_date_active, 'IDR', 'USD') rate_idr_usd
     , prc_tbl.premium
     , CASE WHEN prc_tbl.currency_code = 'USD' THEN (22.0462 * prc_tbl.price) + prc_tbl.premium ELSE prc_tbl.price END unit_price
     , obet.blanket_min_quantity * CASE WHEN prc_tbl.currency_code = 'USD' THEN (22.0462 * prc_tbl.price) + prc_tbl.premium ELSE prc_tbl.price END * SUJ_GET_CLOSING_RATE ( obeh.start_date_active, prc_tbl.currency_code, 'USD')
          total_usd
     , obet.blanket_min_quantity * CASE WHEN prc_tbl.currency_code = 'USD' THEN (22.0462 * prc_tbl.price) + prc_tbl.premium ELSE prc_tbl.price END * SUJ_GET_CLOSING_RATE ( obeh.start_date_active, prc_tbl.currency_code, 'IDR')
          total_idr
     , prc_tbl.ny11
     , prc_tbl.dn_period
     , prc_tbl.contract_nr
     , prc_tbl.contract_date
     , oobha.creation_date
     , oobha.last_update_date
  FROM oe_blanket_headers_all oobha
     , oe_blanket_lines_all oobla
     , oe_blanket_lines_ext obet
     , oe_blanket_headers_ext obeh
     , qp_secu_list_headers_v prl
     , hz_parties party
     , hz_cust_accounts cust_acct
     , hz_cust_site_uses_all bill_su
     , hz_cust_site_uses_all ship_su
     -- harga
     , (  SELECT trans_no
               , price
               , premium
               , ny11
               , dn_period
               , contract_nr
               , contract_date
               , currency_code
            FROM suj_om_prc_cust_tbl
        GROUP BY trans_no
               , price
               , premium
               , ny11
               , dn_period
               , contract_nr
               , contract_date
               , currency_code) prc_tbl
 WHERE     oobha.price_list_id = prl.list_header_id
       AND oobha.sold_to_org_id = cust_acct.cust_account_id(+)
       AND cust_acct.party_id = party.party_id(+)
       AND oobha.invoice_to_org_id = bill_su.site_use_id(+)
       AND oobha.ship_to_org_id = ship_su.site_use_id(+)
       AND oobla.header_id = oobha.header_id
       AND obet.line_id = oobla.line_id
       AND obeh.order_number = oobha.order_number
       AND oobha.sales_document_name = prc_tbl.trans_no

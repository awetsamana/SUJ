/* Formatted on 11/28/2017 4:55:00 PM (QP5 v5.256.13226.35510) */
-- Master Ship To Location 3.1

SELECT acct.cust_account_id customer_account_id
     , site.location ship_to_location
     , site.site_use_id ship_to_location_id
     , party_site.party_site_number
     , acct.status site_status
     , site.site_use_code
     , party_site.attribute_category
     , party_site.attribute1
     , party_site.attribute2
     , party_site.attribute3
     , party_site.attribute4
     , party_site.attribute5
     , party_site.attribute6
     , party_site.attribute7
     , party_site.attribute8
     , party_site.attribute9
     , party_site.attribute10
     , party_site.attribute11
     , party_site.attribute12
     , party_site.attribute13
     , party_site.attribute14
     , party_site.attribute15
     , loc.address1
     , loc.address2
     , loc.address3
     , loc.postal_code
     , loc.city
     , NVL (loc.state, loc.province) province
     , DECODE (loc.country, 'ID', 'INDONESIA', loc.country) country
     , site.creation_date site_creation_date
     , site.last_update_date site_last_update_date
  FROM hz_cust_acct_sites_all acct
     , hz_cust_site_uses_all site
     , hz_party_sites party_site
     , hz_locations loc
 WHERE     acct.cust_acct_site_id = site.cust_acct_site_id
       AND site.site_use_code = 'SHIP_TO'
       AND acct.org_id = site.org_id
       AND loc.location_id = party_site.location_id
       AND acct.party_site_id = party_site.party_site_id;
/* Formatted on 11/27/2017 4:03:47 PM (QP5 v5.256.13226.35510) */
SELECT rct.trx_number invoice_no
     , rct.customer_trx_id invoice_id
     , acr.receipt_number receipt_number
     , acr.cash_receipt_id
     , acr.creation_date receipt_creation_date
     , acr.receipt_date
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN ara.amount_applied * rct.exchange_rate ELSE ara.amount_applied END receipt_amount_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN ara.amount_applied ELSE ara.amount_applied * suj_get_closing_rate ( crh_first_posted.gl_date, 'USD', 'IDR') END
          receipt_amount_idr
     , acr.currency_code
     , CASE WHEN acr.currency_code = 'IDR' THEN acr.exchange_rate ELSE suj_get_closing_rate ( crh_first_posted.gl_date, 'IDR', 'USD') END rate_idr_usd
     , CASE WHEN acr.currency_code = 'IDR' THEN 1 / acr.exchange_rate ELSE suj_get_closing_rate ( crh_first_posted.gl_date, 'USD', 'IDR') END rate_usd_idr
     , acr.creation_date
     , acr.last_update_date
  FROM ar_receivable_applications_all ara
     , ar_cash_receipts_all acr
     , ra_customer_trx_all rct
     , ar_cash_receipt_history_all crh_first_posted
 WHERE     ara.status = 'APP'
       AND ara.cash_receipt_id = acr.cash_receipt_id
       AND ara.applied_customer_trx_id = rct.customer_trx_id
       AND crh_first_posted.cash_receipt_id(+) = acr.cash_receipt_id
       AND crh_first_posted.org_id(+) = acr.org_id
       AND crh_first_posted.first_posted_record_flag(+) = 'Y'
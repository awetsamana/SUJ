/* Formatted on 11/27/2017 6:08:10 PM (QP5 v5.256.13226.35510) */
SELECT mvh.request_number mo_number
     , mvh.header_id mo_header_id
     , wda.delivery_id
     , mvl.line_number mo_line
     , mvl.line_id mo_line_id
     , mp.organization_code inv_organization
     , msib.concatenated_segments item_code
     , mmt.transaction_quantity confirmed_quantity
     , mmt.transaction_uom
     , mmt.transaction_date confirmed_date
     , mmt.transaction_id
     , oel.unit_selling_price
     , CASE
          WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate
          ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'IDR', 'USD')
       END
          rate_idr_usd
     , CASE
          WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate
          ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR')
       END
          rate_usd_idr
     ,   CASE WHEN oeh.transactional_curr_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END
       * oel.unit_selling_price
       * oel.ordered_quantity
          amount_idr
     , CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE 1 END * oel.unit_selling_price * oel.ordered_quantity amount_usd
     , wnd.attribute_category
     , wnd.attribute1
     , wnd.attribute2
     , wnd.attribute3
     , wnd.attribute4
     , wnd.attribute5
     , wnd.attribute6
     , wnd.attribute7
     , wnd.attribute8
     , wnd.attribute9
     , wnd.attribute10
     , mmt.creation_date
     , mmt.last_update_date
  FROM mtl_material_transactions mmt
     , wsh_delivery_details wdd
     , wsh_delivery_assignments wda
     , mtl_txn_request_lines mvl
     , mtl_txn_request_headers mvh
     , mtl_parameters mp
     , mtl_system_items_kfv msib
     , oe_order_lines_all oel
     , oe_order_headers_all oeh
     , gl_daily_rates_v gdr
     , wsh_new_deliveries wnd
 WHERE     mmt.transaction_type_id = 33
       AND mmt.picking_line_id = wdd.delivery_detail_id
       AND mvl.line_id = wdd.move_order_line_id
       AND mvl.header_id = mvh.header_id
       AND wdd.move_order_line_id = mvl.line_id
       AND wdd.delivery_detail_id = wda.delivery_detail_id
       AND mp.organization_id = mmt.organization_id
       AND msib.organization_id = mmt.organization_id
       AND msib.inventory_item_id = mmt.inventory_item_id
       AND wdd.source_line_id = oel.line_id
       AND wdd.source_header_id = oel.header_id
       AND oel.header_id = oeh.header_id
       AND oeh.transactional_curr_code = gdr.from_currency(+)
       AND gdr.from_currency(+) = 'IDR'
       AND gdr.to_currency(+) = 'USD'
       AND oeh.conversion_type_code = gdr.conversion_type(+)
       AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
       AND wnd.delivery_id = wda.delivery_id
/* Formatted on 1/4/2018 10:18:44 AM (QP5 v5.256.13226.35510) */
SELECT oeh.order_number so_number
     , oeh.header_id so_header_id
     , oel.line_number so_line
     , oel.line_id so_line_id
     , mvh.request_number mo_number
     , mvh.header_id mo_header_id
     , mvl.line_number mo_line
     , mvl.line_id mo_line_id
     , mp.organization_code inv_organization
     , msib.concatenated_segments item_code
     , mmt.transaction_quantity
     , mmt.transaction_uom
     , mmt.transaction_date
     , mmt.subinventory_code staging_subinventory
     , mmt.transfer_subinventory subinventory
     , mmt.transaction_id
     , oeh.transactional_curr_code currency_code
     , oel.unit_selling_price
     , CASE
          WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate
          ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'IDR', 'USD')
       END
          rate_idr_usd
     , CASE
          WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.inverse_conversion_rate
          ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR')
       END
          rate_usd_idr
     ,   CASE WHEN oeh.transactional_curr_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)), 'USD', 'IDR') END
       * oel.unit_selling_price
       * oel.ordered_quantity
          amount_idr
     , CASE WHEN oeh.transactional_curr_code = 'IDR' THEN gdr.conversion_rate ELSE 1 END * oel.unit_selling_price * oel.ordered_quantity amount_usd
     , DECODE (wdd.released_status, 'Y', 'N', 'Y') confirmed
     , mvh.creation_date header_creation_date
     , mvh.last_update_date header_last_update_date
     , mvl.creation_date line_creation_date
     , mvl.last_update_date line_last_update_date
  FROM Mtl_material_transactions mmt
     , mtl_txn_request_lines mvl
     , mtl_txn_request_headers mvh
     , oe_order_headers_all oeh
     , oe_order_lines_all oel
     , mtl_parameters mp
     , mtl_system_items_kfv msib
     , gl_daily_rates_v gdr
     , wsh_delivery_details wdd
 WHERE     mmt.transaction_type_id = 52
       AND mmt.move_order_line_id = mvl.line_id
       AND mvl.header_id = mvh.header_id
       AND oeh.header_id = oel.header_id
       AND oel.line_id = mmt.trx_source_line_id
       AND SIGN ( mmt.transaction_quantity) = 1
       AND mp.organization_id = mmt.organization_id
       AND msib.organization_id = mmt.organization_id
       AND msib.inventory_item_id = mmt.inventory_item_Id
       AND oeh.transactional_curr_code = gdr.from_currency(+)
       AND gdr.from_currency(+) = 'IDR'
       AND gdr.to_currency(+) = 'USD'
       AND oeh.conversion_type_code = gdr.conversion_type(+)
       AND NVL (TRUNC ( oeh.conversion_rate_date), TRUNC ( oeh.request_date)) = TRUNC ( gdr.conversion_date(+))
       AND wdd.move_order_line_id(+) = mvl.line_id
UNION ALL
-- 4Jan17 - Add Data Dummy DropShipment from Invoice Dropshipment
SELECT NULL so_number
     , NULL so_header_id
     , NULL so_line
     , TO_NUMBER ( rctl.interface_line_attribute6) so_line_id
     , NULL mo_number
     , NULL mo_header_id
     , NULL mo_line
     , NULL mo_line_id
     , NULL inv_organization
     , NULL item_code
     , NULL transaction_quantity
     , NULL transaction_uom
     , NULL transaction_date
     , NULL staging_subinventory
     , NULL subinventory
     , NULL transaction_id
     , NULL currency_code
     , NULL unit_selling_price
     , NULL rate_idr_usd
     , NULL rate_usd_idr
     , NULL amount_idr
     , NULL amount_usd
     , 'Y' confirmed
     , rct.creation_date header_creation_date
     , rct.last_update_date header_last_update_date
     , rctl.creation_date line_creation_date
     , rctl.last_update_date line_last_update_date
  FROM ra_customer_trx_all rct, ra_customer_trx_lines_all rctl, ra_cust_trx_types_all rctype
 WHERE     rctl.customer_trx_id = rct.customer_trx_id
       AND rctl.interface_line_context = 'ORDER ENTRY'
       AND rctl.line_type <> 'TAX'
       AND rctype.name NOT IN ('Time Deposit', 'Time Deposit 2017', 'Deposit For LC')
       AND rctype.cust_trx_type_id = rct.cust_trx_type_id
       AND rctype.TYPE = 'INV'       
       AND EXISTS
              (SELECT 1
                 FROM oe_order_lines_all ool, oe_order_headers_all ooh
                WHERE     ooh.header_id = ool.header_id
                      AND rctl.interface_line_attribute1 = TO_CHAR ( ooh.order_number)
                      AND rctl.interface_line_attribute6 = TO_CHAR ( ool.line_id)
                      AND ool.source_type_code = 'EXTERNAL')
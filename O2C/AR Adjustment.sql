SELECT rct.customer_trx_id
       , rct.trx_number
       , rct.invoice_currency_code
       , ada.apply_date
       , DECODE (ada.status,  'A', 'Approved',  'R', 'Rejected',  'Others') status
       , SUM ( NVL (ada.line_adjusted, 0)) amount
       , SUM ( NVL (ada.tax_adjusted, 0)) tax
       , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE suj_get_closing_rate ( ada.gl_date, 'IDR', 'USD') END rate_idr_usd
       , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 / rct.exchange_rate ELSE suj_get_closing_rate ( ada.gl_date, 'USD', 'IDR') END rate_usd_idr
       , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( ada.gl_date, 'IDR', 'USD') END * SUM ( NVL (ada.line_adjusted, 0)) amount_idr
       , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE 1 END * SUM ( NVL (ada.line_adjusted, 0)) amount_usd
       , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( ada.gl_date, 'IDR', 'USD') END * SUM ( NVL (ada.tax_adjusted, 0)) tax_idr
       , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE 1 END * SUM ( NVL (ada.tax_adjusted, 0)) tax_usd
    FROM ar_adjustments_all ada, ra_customer_trx_all rct
   WHERE rct.customer_trx_id = ada.customer_trx_id
AND RCT.CUST_TRX_TYPE_ID not in (select CUST_TRX_TYPE_ID from ra_cust_trx_types_all where name IN ('Time Deposit', 'Time Deposit 2017', 'Deposit For LC'))
GROUP BY rct.customer_trx_id
       , rct.trx_number
       , rct.invoice_currency_code
       , rct.exchange_rate
       , ada.gl_date
       , ada.apply_date
       , ada.status;
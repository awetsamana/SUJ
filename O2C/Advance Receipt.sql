/* Formatted on 11/28/2017 4:49:22 PM (QP5 v5.256.13226.35510) */
--
--SELECT acr.attribute1 sales_order_number
--     , rct.trx_number invoice_no
--     , rct.creation_date invoice_creation_date
--     , rct.customer_trx_id invoice_id
--     , acr.receipt_number receipt_number
--     , acr.cash_receipt_id
--     , acr.receipt_date
--     , ara.amount_applied receipt_amount
--     , acr.currency_code
--     , acr.exchange_rate
--     , gdt.description rate_type
--     , acr.exchange_date
--     , acr.creation_date
--     , acr.last_update_date
--  FROM ar_receivable_applications_all ara
--     , ar_cash_receipts_all acr
--     , ra_customer_trx_all rct
--     , gl_daily_conversion_types gdt
-- WHERE     gdt.conversion_type(+) = acr.exchange_rate_type
--       AND ara.status = 'APP'
--       AND ara.cash_receipt_id = acr.cash_receipt_id
--       AND ara.applied_customer_trx_id = rct.customer_trx_id
--       AND acr.attribute1 IS NOT NULL

SELECT rct.customer_trx_id
     , rct.trx_number invoice_number
     , rctl.interface_line_attribute1 order_number
     , rct.invoice_currency_code
     , rctl.unit_selling_price
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'IDR', 'USD') END rate_idr_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 / rct.exchange_rate ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END rate_usd_idr
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rctl.extended_amount * rct.exchange_rate ELSE rctl.extended_amount END invoice_amount_usd
     , CASE WHEN rct.invoice_currency_code = 'IDR' THEN rctl.extended_amount ELSE rctl.extended_amount * suj_get_closing_rate ( rct.trx_date, 'USD', 'IDR') END invoice_amount_idr
     , (rctl.tax_recoverable) * CASE WHEN rct.invoice_currency_code = 'IDR' THEN rct.exchange_rate ELSE 1 END tax_recoverable_usd
     , (rctl.tax_recoverable) * CASE WHEN rct.invoice_currency_code = 'IDR' THEN 1 ELSE suj_get_closing_rate ( gd.gl_date, 'USD', 'IDR') END tax_recoverable_idr
     , DECODE (rcv.applied_customer_trx_id, NULL, 'N', 'Y') is_received
     , rct.creation_date header_creation_date
     , rct.last_update_date header_last_update_date
     , rctl.creation_date line_creation_date
     , rctl.last_update_date line_last_update_date
  FROM ra_customer_trx_all rct
     , ra_customer_trx_lines_all rctl
     , ra_batch_sources_all rbsa
     , ra_cust_trx_types_all rctt
     , ra_cust_trx_line_gl_dist_all gd
     , (  SELECT ara.applied_customer_trx_id
            FROM ar_receivable_applications_all ara, ar_cash_receipts_all acr
           WHERE ara.status = 'APP' AND ara.cash_receipt_id = acr.cash_receipt_id(+)
        GROUP BY ara.applied_customer_trx_id) rcv
 WHERE     rct.customer_trx_id = rctl.customer_trx_id
       AND rbsa.BATCH_SOURCE_ID = rct.BATCH_SOURCE_ID
       AND rbsa.name = 'SUJ SO SPS'
       AND rctt.CUST_TRX_TYPE_ID = rct.CUST_TRX_TYPE_ID
       AND rcv.applied_customer_trx_id(+) = rctl.customer_trx_id
       AND rctl.line_type <> 'TAX'
       AND rct.customer_trx_id = gd.customer_trx_id
       AND 'REC' = gd.account_class
       AND 'Y' = gd.latest_rec_flag;
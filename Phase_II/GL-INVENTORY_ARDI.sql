WITH cogm_category
        AS (SELECT mic.inventory_item_id, mic.organization_id, mc.concatenated_segments cogm_category
              FROM mtl_item_categories mic, mtl_categories_kfv mc, mtl_category_sets mics
             WHERE     mic.category_id = mc.category_id
                   AND mc.STRUCTURE_ID = mics.STRUCTURE_ID
                   AND mics.category_set_name = 'SUJ_COGM_Category'
                   AND mic.category_set_id = mics.category_set_id)
   , inv_category
        AS (SELECT mic.inventory_item_id, mic.organization_id, mc.concatenated_segments inv_category
              FROM mtl_item_categories mic, mtl_categories_kfv mc, mtl_category_sets mics
             WHERE     mic.category_id = mc.category_id
                   AND mc.STRUCTURE_ID = mics.STRUCTURE_ID
                   AND mics.category_set_name = 'SUJ_Inventory_Category'
                   AND mic.category_set_id = mics.category_set_id)
SELECT xet.application_id
     , TO_CHAR (mmt.transaction_date, 'MON-YY') period_name
     , mmt.transaction_date trx_date
     , mmt.transaction_id trx_id
     , haou.name warehouse
     , xect.name trx_type
     , xet.name event_type
     , msi.segment1 item_code
     , cogmc.cogm_category
     , invc.inv_category
     , mmt.PRIMARY_QUANTITY qty
     , MTA.RATE_OR_AMOUNT transaction_cost
     , gl.CURRENCY_CODE transaction_currency
     , CASE WHEN mta.CURRENCY_CONVERSION_RATE <> -1 THEN mta.CURRENCY_CONVERSION_RATE ELSE 1 END rate
     , gcc.concatenated_segments COA
     , xah.gl_transfer_status_code transfer_to_gl
     , gjh.status posted_flag
     , gcc.segment1
     , gcc.segment2
     , gcc.segment3
     , gcc.segment4
     , gcc.segment5
     , gcc.segment6
     , gcc.segment7
     , xal.entered_dr
     , xal.entered_cr
     , xal.accounted_dr
     , xal.accounted_cr
     , xal.accounting_class_code accounting_class
     , b.name journal_batch_name
     , gjh.name journal_name
     , gjl.description journal_line_desc
     , mmt.creation_date
  FROM hr_all_organization_units haou
     , mtl_material_transactions mmt
     , mtl_transaction_accounts mta
     , mtl_transaction_types mtt
     , mtl_system_items msi
     , cogm_category cogmc
     , inv_category invc
     , xla_distribution_links xdl
     , xla_events xe
     , xla_event_types_vl xet
     , xla_event_classes_vl xect
     , xla_ae_headers xah
     , xla_ae_lines xal
     , gl_je_lines gjl
     , gl_je_headers gjh
     , gl_je_batches b
     , gl_ledgers gl
     , gl_code_combinations_kfv gcc
 WHERE     haou.organization_id = mmt.organization_id
       AND mmt.transaction_id = mta.transaction_id
       AND mmt.organization_id = mta.organization_id
       AND mmt.transaction_type_id = mtt.transaction_type_id
       AND mmt.inventory_item_id = msi.inventory_item_id
       AND mmt.organization_id = msi.organization_id
       AND mmt.inventory_item_id = cogmc.inventory_item_id(+)
       AND mmt.organization_id = cogmc.organization_id(+)
       AND mmt.inventory_item_id = invc.inventory_item_id(+)
       AND mmt.organization_id = invc.organization_id(+)
       AND mta.inv_sub_ledger_id = xdl.SOURCE_DISTRIBUTION_ID_NUM_1
       AND xdl.event_id = xe.event_id
       AND xdl.source_distribution_type = 'MTL_TRANSACTION_ACCOUNTS'
       AND xdl.application_id = xe.application_id
       AND xet.application_id = xe.application_id
       AND xet.event_type_code = xe.event_type_code
       AND xect.application_id = xet.application_id
       AND xect.entity_code = xet.entity_code
       AND xect.event_class_code = xet.event_class_code
       AND xe.event_id = xah.event_id
       AND xdl.ae_header_id = xal.ae_header_id
       AND xdl.ae_line_num = xal.ae_line_num
       AND xdl.application_id = xal.application_id
       AND xal.gl_sl_link_id = gjl.gl_sl_link_id
       AND xal.code_combination_id = gjl.code_combination_id
       AND gjl.je_header_id = gjh.je_header_id
       AND gjh.je_batch_id = b.je_batch_id
       AND mta.reference_account = gcc.code_combination_id
       AND gjh.ledger_id = gl.ledger_id(+)
       --AND TO_CHAR (mmt.transaction_date, 'MON-YY') = :p_period_name
UNION ALL
select xet.application_id
     , to_char( rrsl.accounting_date , 'MON-YY' ) period_name 
     , rrsl.accounting_date trx_date 
     , rt.transaction_id trx_id
     , haou.name warehouse
     , xect.name trx_type
     , xet.name event_type
     , msi.segment1 item_code
     , cogmc.cogm_category
     , invc.inv_category
     , rt.primary_quantity qty
     , rrsl.currency_conversion_rate transaction_cost
     , gl.CURRENCY_CODE transaction_currency
     , NULL rate
     , gcc.concatenated_segments COA
     , xah.gl_transfer_status_code transfer_to_gl
     , gjh.status posted_flag
     , gcc.segment1
     , gcc.segment2
     , gcc.segment3
     , gcc.segment4
     , gcc.segment5
     , gcc.segment6
     , gcc.segment7
     , xal.entered_dr
     , xal.entered_cr
     , xal.accounted_dr
     , xal.accounted_cr
     , xal.accounting_class_code accounting_class
     , b.name journal_batch_name
     , gjh.name journal_name
     , gjl.description journal_line_desc
     , rrsl.creation_date
      from hr_all_organization_units haou
           , rcv_transactions rt 
           , rcv_shipment_lines rsl
           , mtl_system_items msi 
           , cogm_category cogmc
           , inv_category invc
           , RCV_RECEIVING_SUB_LEDGER rrsl
           , xla_distribution_links xdl
           , xla_events xe
           , xla_event_types_vl xet
           , xla_event_classes_vl xect
           , xla_ae_headers xah
           , xla_ae_lines xal
           , gl_je_lines gjl
           , gl_je_headers gjh
           , gl_je_batches b
           , gl_ledgers gl
           , gl_code_combinations_kfv gcc
     where haou.organization_id = msi.organization_id
       and rt.shipment_line_id = rsl.shipment_line_id 
       and rt.po_header_id = rsl.po_header_id 
       and rt.po_line_id = rsl.po_line_id 
       and rt.shipment_header_id = rsl.shipment_header_id
       and rsl.item_id = msi.inventory_item_id 
       and rt.organization_id  = msi.organization_id 
       AND rsl.item_id = cogmc.inventory_item_id(+)
       AND rsl.TO_ORGANIZATION_ID = cogmc.organization_id(+)
       AND rsl.item_id = invc.inventory_item_id(+)
       AND rsl.TO_ORGANIZATION_ID = invc.organization_id(+)
       and rt.transaction_id = rrsl.rcv_transaction_id
       and rrsl.RCV_SUB_LEDGER_ID = xdl.SOURCE_DISTRIBUTION_ID_NUM_1
       AND xdl.source_distribution_type = 'RCV_RECEIVING_SUB_LEDGER'
       and xdl.event_id = xe.event_id
       and xet.application_id = xe.application_id
       AND xet.event_type_code = xe.event_type_code
       AND xect.application_id = xet.application_id
       AND xect.entity_code = xet.entity_code
       AND xect.event_class_code(+) = xet.event_class_code
       AND xe.event_id = xah.event_id
       AND xdl.ae_header_id = xal.ae_header_id
       AND xdl.ae_line_num = xal.ae_line_num
       AND xdl.application_id = xal.application_id
       AND xal.gl_sl_link_id = gjl.gl_sl_link_id
       AND xal.code_combination_id = gjl.code_combination_id
       AND gjl.je_header_id = gjh.je_header_id
       AND gjh.je_batch_id = b.je_batch_id
       AND gjh.ledger_id = gl.ledger_id
       AND rrsl.code_combination_id = gcc.code_combination_id
       --AND to_char( rrsl.accounting_date , 'MON-YY' ) = :p_period_name
UNION ALL
SELECT xet.application_id
     , TO_CHAR (wta.transaction_date, 'MON-YY') period_name
     , wta.transaction_date trx_date
     , wta.transaction_id trx_id
     , haou.name warehouse
     , xect.name trx_type
     , xet.name event_type
     , NULL item_code
     , NULL inventory_category
     , NULL cogm_category
     , NULL qty
     , wta.base_transaction_value transaction_cost
     , gl.CURRENCY_CODE transaction_currency
     , 1 rate
     , gcc.CONCATENATED_SEGMENTS COA
     , xah.gl_transfer_status_code transfer_to_gl
     , gjh.status posted_flag
     , gcc.segment1
     , gcc.segment2
     , gcc.segment3
     , gcc.segment4
     , gcc.segment5
     , gcc.segment6
     , gcc.segment7
     , xal.entered_dr
     , xal.entered_cr
     , xal.accounted_dr
     , xal.accounted_cr
     , xal.accounting_class_code accounting_class
     , b.name journal_batch_name
     , gjh.name journal_name
     , gjl.description journal_line_desc
     , wt.creation_date
  FROM hr_all_organization_units haou
     , wip_transaction_accounts wta
     , wip_transactions wt
     , xla_distribution_links xdl
     , xla_events xe
     , xla_event_types_tl xet
     , xla_event_classes_tl xect
     , xla_ae_headers xah
     , xla_ae_lines xal
     , gl_je_lines gjl
     , gl_je_headers gjh
     , gl_je_batches b
     , gl_ledgers gl
     , mfg_lookups lu1
     , gl_code_combinations_kfv gcc
 WHERE     haou.organization_id = wta.organization_id
       AND wta.transaction_id = wt.transaction_id
       AND lu1.lookup_type(+) = 'WIP_TRANSACTION_TYPE'
       AND lu1.lookup_code(+) = wt.transaction_type
       AND wta.reference_account = gcc.code_combination_id
       AND wta.wip_sub_ledger_id = xdl.SOURCE_DISTRIBUTION_ID_NUM_1
       AND xdl.source_distribution_type = 'WIP_TRANSACTION_ACCOUNTS'
       AND xdl.event_id = xe.event_id
       AND xdl.application_id = xe.application_id
       AND xet.application_id = xe.application_id
       AND xet.event_type_code = xe.event_type_code
       AND xet.language = USERENV ('LANG')
       AND xect.application_id = xet.application_id
       AND xect.entity_code = xet.entity_code
       AND xect.event_class_code = xet.event_class_code
       AND xect.language = USERENV ('LANG')
       AND xe.event_id = xah.event_id
       AND xdl.ae_header_id = xal.ae_header_id
       AND xdl.ae_line_num = xal.ae_line_num
       AND xdl.application_id = xal.application_id
       AND xal.gl_sl_link_id = gjl.gl_sl_link_id
       AND xal.code_combination_id = gjl.code_combination_id
       AND gjl.je_header_id = gjh.je_header_id
       AND gjh.je_batch_id = b.je_batch_id
       AND gjh.ledger_id = gl.ledger_id
       --AND TO_CHAR (wta.transaction_date, 'MON-YY') = :p_period_name
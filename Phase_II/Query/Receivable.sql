SELECT trx.trx_number,
       trx.cust_trx_type_id,
       trx.bill_to_customer_id,
       trx_dist.amount ar_inv_dist_amount,
       trx_dist.code_combination_id ar_inv_dist_ccid,
          gcc.segment1AR 
       || '.'
       || gcc.segment2
       || '.'
       || gcc.segment3
       || '.'
       || gcc.segment4
       || '.'
       || gcc.segment5
       || '.'
       || gcc.segment6
       || '.'
       || gcc.segment7
       || '.'
       || gcc.segment8
          ar_inv_dist_code_combination,
       gjh.name gl_je_header_name,
       gjh.description gl_je_header_desc,
       gjl.je_line_num,
       gjl.code_combination_id gl_je_line_ccid
  FROM ar.ra_customer_trx_all trx,
       ar.ra_cust_trx_line_gl_dist_all trx_dist,
       gl.gl_code_combinations gcc,
       gl.gl_ledgers gl,
       xla.xla_transaction_entities xte,
       xla.xla_ae_headers xah,
       xla.xla_ae_lines xal,
       xla.xla_distribution_links xdl,
       gl.gl_import_references gir,
       gl.gl_je_headers gjh,
       gl.gl_je_lines gjl
 WHERE 1 = 1
   -- AND trx.org_id = :P_ORG_ID
   -- AND trx.batch_source_id = :P_BATCH_SOURCE_ID
   AND trx.customer_trx_id = :P_CUSTOMER_TRX_ID
   AND trx.customer_trx_id = trx_dist.customer_trx_id
   AND gcc.code_combination_id = trx_dist.code_combination_id
   --  AND gl.short_name = :P_LEDGER_SHORT_NAME
   AND gl.chart_of_accounts_id = gcc.chart_of_accounts_id
   AND xte.ledger_id = gl.ledger_id
   AND xte.entity_code = 'TRANSACTIONS'
   AND xte.application_id = 222
   AND xte.source_id_int_1 = trx.customer_trx_id
   AND xah.entity_id = xte.entity_id
   AND xal.ae_header_id = xah.ae_header_id
   AND xal.accounting_class_code = 'REVENUE'
   AND xdl.ae_header_id = xah.ae_header_id
   AND xdl.ae_line_num = xal.ae_line_num
   AND xdl.source_distribution_id_num_1 = trx_dist.cust_trx_line_gl_dist_id
   AND xdl.application_id = 222
   AND gir.gl_sl_link_id = xal.gl_sl_link_id
   AND gjh.je_header_id = gir.je_header_id
   AND gjl.je_header_id = gjh.je_header_id
   AND gjl.je_line_num = gir.je_line_num;
   
   --receipt_transaction
   SELECT cr.org_id,
       ou.name operating_unit,
       cr.status,
       cr.receipt_number,
       cr.TYPE,
       cr.receipt_date,
       cr.currency_code,
       cr.amount receipt_amount,
       rec_hist.ACCTD_AMOUNT funct_amount,
       party.party_name customer_name,
       cr.deposit_date,
       cr.receipt_method_id,
       rec_method.name receipt_method,
       rec_hist.gl_date,
       CE_BANK_AND_ACCOUNT_UTIL.GET_MASKED_BANK_ACCT_NUM (
          REMIT_BANK.BANK_ACCOUNT_ID)
          REMIT_BANK_ACCOUNT,
           BB.BANK_NAME REMIT_BANK_NAME,
           bb.bank_branch_name branch
  FROM AR_CASH_RECEIPTS cr,
       AR_RECEIPT_METHODS REC_METHOD,
       AR_CASH_RECEIPT_HISTORY_ALL rec_hist,
       CE_BANK_ACCOUNTS CBA,
       CE_BANK_ACCT_USES_OU REMIT_BANK,
       CE_BANK_BRANCHES_V BB,
       hr_operating_units ou,
       HZ_CUST_ACCOUNTS CUST,
       HZ_PARTIES party
 WHERE   1=1--  cr.receipt_number = '01'
       AND cr.org_id = ou.organization_id
       AND CR.PAY_FROM_CUSTOMER = CUST.CUST_ACCOUNT_ID(+)
       AND CUST.PARTY_ID = PARTY.PARTY_ID(+)
       AND CR.RECEIPT_METHOD_ID = REC_METHOD.RECEIPT_METHOD_ID
       AND rec_hist.CASH_RECEIPT_ID(+) = CR.CASH_RECEIPT_ID
       AND rec_hist.ORG_ID(+) = CR.ORG_ID
       and rec_hist.FIRST_POSTED_RECORD_FLAG(+) = 'Y'
       AND REMIT_BANK.BANK_ACCT_USE_ID(+) = CR.REMIT_BANK_ACCT_USE_ID
       AND REMIT_BANK.ORG_ID(+) = CR.ORG_ID
       AND remit_bank.bank_account_id = CBA.bank_account_id(+)
       AND BB.BRANCH_PARTY_ID(+) = CBA.BANK_BRANCH_ID

	   --AR transaction
	   /* Formatted on 8/6/2018 4:46:08 PM (QP5 v5.256.13226.35538) */
SELECT OU.NAME ORGANIZATION_NAME,
       RAH.TRX_NUMBER,
       RAH.COMPLETE_FLAG,
       TO_CHAR (RAH.TRX_DATE, 'DD-MON-RR') TRX_DATE,
       TO_CHAR (RAH.TERM_DUE_DATE, 'DD-MON-RR') DUE_DATE,
       RAH.DOC_SEQUENCE_VALUE DOCUMENT_NUM,
       RAT.NAME,
       RAT.TYPE,
       HP_BILL.PARTY_NAME BILL_PARTY_NAME,
       HP_BILL.ORGANIZATION_NAME_PHONETIC BILL_ORG_NAME,
       HP_BILL.TAX_REFERENCE BILL_NPWP_NUMBER,
       HCA_BILL.ACCOUNT_NUMBER BILL_ACCOUNT_NUMBER,
       RAH.BILL_TO_SITE_USE_ID,
       HZCSU_BILL.LOCATION BILL_LOCATION,
          HP_BILL.ADDRESS1
       || ', '
       || HP_BILL.ADDRESS2
       || ', '
       || HP_BILL.ADDRESS3
       || ', '
       || HP_BILL.ADDRESS4
       || ', '
       || HP_BILL.CITY
       || ', '
       || HP_BILL.PROVINCE
       || ', '
       || HP_BILL.POSTAL_CODE
       || ', '
       || FT_BILL.TERRITORY_SHORT_NAME
          ALAMAT,
       HZCSU_BILL.CONTACT_ID,
       HZCON_BILL.CONTACT_NUMBER CONTACT,
       HP_SOLD.PARTY_NAME SOLD_TO_PARTY_NAME,
       HCA_SOLD.ACCOUNT_NUMBER SOLD_ACCOUNT_NUMBER,
       HP_PAY.PARTY_NAME PAY_TO_PARTY_NAME,
       HCA_SOLD.ACCOUNT_NUMBER PAY_ACOUNT_NUMBER,
       HZCSU_PAY.LOCATION PAY_LOCATION,
       (SELECT SUM (RAL.EXTENDED_AMOUNT)
          FROM RA_CUSTOMER_TRX_LINES_ALL RAL
         WHERE     RAL.CUSTOMER_TRX_ID = RAH.CUSTOMER_TRX_ID
               AND RAL.LINE_TYPE = 'LINE')
          LINE_AMOUNT,
       (SELECT SUM (RAL.EXTENDED_AMOUNT)
          FROM RA_CUSTOMER_TRX_LINES_ALL RAL
         WHERE     RAL.CUSTOMER_TRX_ID = RAH.CUSTOMER_TRX_ID
               AND RAL.LINE_TYPE = 'TAX')
          LINE_TAX,
       (SELECT SUM (RAL.EXTENDED_AMOUNT)
          FROM RA_CUSTOMER_TRX_LINES_ALL RAL
         WHERE RAL.CUSTOMER_TRX_ID = RAH.CUSTOMER_TRX_ID)
          TOTAL_AMOUNT
  FROM RA_CUSTOMER_TRX_ALL RAH,
       RA_CUST_TRX_TYPES_ALL RAT,
       HZ_CUST_ACCOUNTS_ALL HCA_BILL,
       HZ_CUST_ACCOUNTS_ALL HCA_SOLD,
       HZ_CUST_ACCOUNTS_ALL HCA_PAY,
       HZ_PARTIES HP_BILL,
       HZ_PARTIES HP_SOLD,
       HZ_PARTIES HP_PAY,
       HZ_CUST_SITE_USES_ALL HZCSU_BILL,
       HZ_CUST_SITE_USES_ALL HZCSU_SOLD,
       HZ_CUST_SITE_USES_ALL HZCSU_PAY,
       FND_TERRITORIES_TL FT_BILL,
       HZ_ORG_CONTACTS HZCON_BILL,
       HR_ALL_ORGANIZATION_UNITS OU
 WHERE     RAT.CUST_TRX_TYPE_ID = RAH.CUST_TRX_TYPE_ID
       AND RAH.BILL_TO_CUSTOMER_ID = HCA_BILL.CUST_ACCOUNT_ID(+)
       AND HCA_BILL.PARTY_ID = HP_BILL.PARTY_ID(+)
       AND HZCSU_BILL.SITE_USE_ID = RAH.BILL_TO_SITE_USE_ID
       AND FT_BILL.TERRITORY_CODE(+) = HP_BILL.COUNTRY
       AND HZCON_BILL.ORG_CONTACT_ID(+) = HZCSU_BILL.CONTACT_ID
       AND HCA_SOLD.CUST_ACCOUNT_ID = RAH.SOLD_TO_CUSTOMER_ID
       AND HP_SOLD.PARTY_ID = HCA_SOLD.PARTY_ID
       AND RAH.PAYING_CUSTOMER_ID = HCA_PAY.CUST_ACCOUNT_ID(+)
       AND RAH.PAYING_SITE_USE_ID = HZCSU_PAY.SITE_USE_ID(+)
       AND HZCSU_SOLD.SITE_USE_ID(+) = RAH.SOLD_TO_SITE_USE_ID
       AND HP_PAY.PARTY_ID(+) = HCA_PAY.PARTY_ID
       AND OU.ORGANIZATION_ID = RAH.ORG_ID
	   
	   --query Customer 
	   SELECT
  ----------------------
  --Customer Information
  ----------------------
  hp.party_id,
  hp.party_name "CUSTOMER_NAME",
  hca.cust_account_id,
  hca.account_number,
  hcas.org_id,
  ---------------------------
  --Customer Site Information
  ---------------------------
  hcas.cust_acct_site_id,
  hps.party_site_number,
  hcsu.site_use_code,
  -----------------------
  --Customer Site Address
  -----------------------
  hl.address1,
  hl.address2,
  hl.address3,
  hl.address4,
  hl.city,
  hl.postal_code,
  hl.state,
  hl.province,
  hl.county,
  hl.country,
  hl.address_style
FROM hz_parties hp,
  hz_party_sites hps,
  hz_cust_accounts_all hca,
  hz_cust_acct_sites_all hcas,
  hz_cust_site_uses_all hcsu,
  hz_locations hl
WHERE 1                    =1
AND hp.party_id            = hca.party_id
AND hca.cust_account_id    = hcas.cust_account_id(+)
AND hps.party_site_id(+)   = hcas.party_site_id
AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
  --
AND hps.location_id = hl.location_id(+)
  --
AND hp.party_type = 'ORGANIZATION' 
--AND hp.status     = 'A'            
ORDER BY to_number(hp.party_number),
  hp.party_name,
  hca.account_number;

--  Gl - Inventory
SELECT (SELECT meaning
          FROM fnd_lookup_values flv
         WHERE     lookup_type = 'CST_ACCOUNTING_LINE_TYPE'
               AND lookup_code = mta.accounting_line_type)
          TYPE,
       GJB.NAME BATCH_NAME,
       gL.period_name,
       gl.accounted_cr,
       gl.accounted_dr,
       gl.entered_cr,
       gl.entered_dr,
       gh.je_source,
       gh.je_category,
       gh.posted_date,
       (SELECT user_je_category_name
          FROM gl_je_categories
         WHERE je_category_name = gh.je_category)
          user_category_name,
       mta.inv_sub_ledger_id,
       mta.base_transaction_value,
       mta.currency_conversion_rate,
       mta.rate_or_amount,
       mta.currency_code,
       mmt.subinventory_code,
       TRUNC (mmt.transaction_date) transaction_date,
       mmt.transaction_quantity,
       mmt.transaction_uom,
       mmt.primary_quantity,
       mmt.actual_cost,
       mmt.source_code,
       mmt.source_line_id,
       mmt.rcv_transaction_id,
       msi.concatenated_segments item,
       gcc.concatenated_segments GL_ACCOUNT_STRING,
       ood.organization_code,
       ood.organization_name,
       msi.segment1 item_name,
       msi.description
  --         mmt.transaction_id,
  --         ael.ae_header_id,
  --         ael.ae_line_num,
  --         gl.je_header_id,
  --        gl.je_line_num
  --         gcc.code_combination_id,
  --         msi.inventory_item_id,
  --         msi.organization_id,
  --         gjb.je_batch_id
  FROM xla_transaction_entities_upg ent,
       xla_events e,
       xla_distribution_links xld,
       mtl_transaction_accounts mta,
       mtl_material_transactions mmt,
       xla_ae_headers ah,
       xla_ae_lines ael,
       gl_import_references gir,
       gl_je_lines gl,
       gl_code_combinations_kfv gcc,
       gl_je_headers gh,
       GL_JE_BATCHES GJB,
       mtl_system_items_kfv msi,
       org_organization_definitions ood
 WHERE     mmt.transaction_id = NVL (ent.source_id_int_1, -99)
       AND ent.entity_code = 'MTL_ACCOUNTING_EVENTS'
       --AND ent.application_id = 707
       AND ent.entity_id = e.entity_id
       --AND e.application_id = 707
       AND e.event_id = xld.event_id
       --AND ah.application_id = 707
       AND ah.entity_id = ent.entity_id
       AND ah.event_id = e.event_id
       AND ah.ledger_id = ent.ledger_id
       AND ah.ae_header_id = ael.ae_header_id
       --AND ael.application_id = 707
       AND ael.ledger_id = ah.ledger_id
       AND ael.AE_HEADER_ID = xld.AE_HEADER_ID
       AND ael.AE_LINE_NUM = xld.AE_LINE_NUM
       -- AND xld.application_id = 707
       AND xld.source_distribution_type = 'MTL_TRANSACTION_ACCOUNTS'
       AND xld.source_distribution_id_num_1 = mta.inv_sub_ledger_id
       AND mta.transaction_id = mmt.transaction_id
       AND ael.gl_sl_link_id = gir.gl_sl_link_id
       AND ael.gl_sl_link_table = gir.gl_sl_link_table
       AND gir.je_header_id = gl.je_header_id
       AND gir.je_line_num = gl.je_line_num
       AND gl.code_combination_id = gcc.code_Combination_id
       AND gl.je_header_id = gh.je_header_id
       AND GH.JE_BATCH_ID = GJB.JE_BATCH_ID
       AND mta.transaction_id = mmt.transaction_id
       AND mmt.inventory_item_id = msi.inventory_item_id
       AND mmt.organization_id = msi.organization_id
       AND msi.organization_id = ood.organization_id		 
		 
-- Material TRANSACTION
/* Formatted on 8/7/2018 3:09:56 PM (QP5 v5.256.13226.35538) */
  SELECT MSI.SEGMENT1 "ITEM CODE",
         MSI.DESCRIPTION,
         MT.SUBINVENTORY_CODE,
         MIL.INVENTORY_LOCATION_ID,
         WLPN2.LICENSE_PLATE_NUMBER CONTENT_LPN,
         CASE
            WHEN MIL.SEGMENT1 IS NULL
            THEN
               NULL
            ELSE
                  MIL.SEGMENT1
               || '.'
               || MIL.SEGMENT2
               || '.'
               || MIL.SEGMENT3
               || '.'
               || MIL.SEGMENT4
         END
            LOCATOR,
         MT.LPN_ID LPN_ID,
         WLPN.LICENSE_PLATE_NUMBER,
         --UPDATE BY FARID BACHTIAR
         MTLN.SUPPLIER_LOT_NUMBER "SUPPLIER LOT",
         --MT.VENDOR_LOT_NUMBER "SUPPLIER LOT",
         --END UPDATE
         MTLN.LOT_NUMBER INTERNAL_LOT,
         --MLN.EXPIRATION_DATE,
         TO_CHAR (
            (SELECT EXPIRATION_DATE
               FROM MTL_LOT_NUMBERS
              WHERE     LOT_NUMBER = MTLN.LOT_NUMBER
                    AND INVENTORY_ITEM_ID = MTLN.INVENTORY_ITEM_ID
                    AND ORGANIZATION_ID = MTLN.ORGANIZATION_ID),
            'DD-MON-RRRR')
            EXPIRATION_DATE,
         -- MMS.STATUS_CODE,
         (SELECT mms.status_code
            FROM MTL_MATERIAL_STATUSES_TL mms, mtl_lot_numbers mln
           WHERE     mln.status_id = mms.status_id
                 AND mln.lot_number = mtln.lot_number
                 AND mln.inventory_item_id = msi.inventory_item_id
                 AND mln.organization_id = msi.organization_id)
            STATUS_CODE,
         --CHANGE AND REMARK BY FARID
         --MLN.LAST_UPDATED_BY,
         (SELECT FU.USER_NAME
            FROM MTL_LOT_NUMBERS LOT, FND_USER FU
           WHERE     LOT.LOT_NUMBER = MTLN.LOT_NUMBER
                 AND LOT.INVENTORY_ITEM_ID = MTLN.INVENTORY_ITEM_ID
                 AND LOT.ORGANIZATION_ID = MTLN.ORGANIZATION_ID
                 AND FU.USER_ID = LOT.LAST_UPDATED_BY)
            LAST_UPDATED_BY,
         --END
         --MLN.LAST_UPDATE_DATE,
         TO_CHAR (
            (SELECT LAST_UPDATE_DATE
               FROM MTL_LOT_NUMBERS
              WHERE     LOT_NUMBER = MTLN.LOT_NUMBER
                    AND INVENTORY_ITEM_ID = MTLN.INVENTORY_ITEM_ID
                    AND ORGANIZATION_ID = MTLN.ORGANIZATION_ID),
            'DD-MON-RRRR HH24:MI:SS')
            LAST_UPDATE_DATE,
         MTT.TRANSACTION_TYPE_NAME,
         TO_CHAR (MT.TRANSACTION_DATE, 'DD-MON-RRRR HH24:MI:SS')
            TRANSACTION_DATE,
         MT.TRANSACTION_UOM,
         MT.TRANSACTION_QUANTITY,
         MSI.PRIMARY_UOM_CODE,
         MT.PRIMARY_QUANTITY,
         MT.SECONDARY_UOM_CODE,
         MT.SECONDARY_TRANSACTION_QUANTITY,
         MT.TRANSACTION_SOURCE_ID,
         ximti.source SOURCE_NO,
         MTSS.TRANSACTION_SOURCE_TYPE_NAME,
         MT.TRANSACTION_SOURCE_TYPE_ID,
         MT.TRANSACTION_SOURCE_ID,
         CASE
            WHEN MTR.REASON_NAME IS NULL THEN NULL
            ELSE MTR.REASON_NAME || '-' || MTR.DESCRIPTION
         END
            REASON,
         MT.TRANSACTION_REFERENCE,
         MT.SHIPMENT_NUMBER,
         MT.SHIP_TO_LOCATION_ID,
         MT.TRANSFER_SUBINVENTORY,
         MIL.SEGMENT1,
         CASE
            WHEN MIL2.SEGMENT1 IS NULL
            THEN
               NULL
            ELSE
                  MIL2.SEGMENT1
               || '.'
               || MIL2.SEGMENT2
               || '.'
               || MIL2.SEGMENT3
               || '.'
               || MIL2.SEGMENT4
         END
            TRANSFER_LOCATOR,
         MIL.INVENTORY_LOCATION_ID,
         MT.TRANSACTION_ID,
         --UPDATE BY FARID BACHTIAR
         -- MT.CREATED_BY,
         (SELECT USER_NAME
            FROM FND_USER
           WHERE USER_ID = MT.CREATED_BY)
            CREATED_BY,
         --END UPDATE
         MT.CREATION_DATE,
         (SELECT license_plate_number
            FROM wms_license_plate_numbers
           WHERE lpn_id = MT.TRANSFER_LPN_ID)
            TRANSFER_LPN_ID,
         HLA.LOCATION_CODE,
         RSH.RECEIPT_NUM
    FROM MTL_MATERIAL_TRANSACTIONS MT,
         MTL_SYSTEM_ITEMS MSI,
         MTL_ITEM_LOCATIONS MIL,
         MTL_ITEM_LOCATIONS MIL2,
         MTL_PARAMETERS MP,
         MTL_TRANSACTION_LOT_NUMBERS MTLN,
         --
         --MTL_LOT_NUMBERS MLN,
         MTL_MATERIAL_STATUSES MMS,
         MTL_TRANSACTION_TYPES MTT,
         MTL_TXN_SOURCE_TYPES MTSS,
         MTL_TRANSACTION_REASONS MTR,
         --RCV_TRANSACTIONS RT,
         WMS_LICENSE_PLATE_NUMBERS WLPN,
         --LPN TRANSFER
         WMS_LICENSE_PLATE_NUMBERS WLPN2,
         RCV_SHIPMENT_HEADERS RSH,
         RCV_TRANSACTIONS RT,
         PO_HEADERS_ALL POH,
         --ADDED BY FARID BACHTIAR
         FND_USER USR,
         --sales order
         MTL_SALES_ORDERS MSO,
         --move order
         mtl_txn_request_headers mtrh,
         (SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 'null' receipt_num,
                 mmt.transaction_source_name "SOURCE",
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 MTL_TXN_SOURCE_TYPES tst
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME NOT IN ('Move order',
                                                              'Purchase order',
                                                              'Job or Schedule',
                                                              'Internal order',
                                                              'Sales order',
                                                              'RMA',
                                                              'Internal requisition')
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 'null' receipt_num,
                 moh.request_number,
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 MTL_TXN_SOURCE_TYPES tst,
                 MTL_TXN_REQUEST_HEADERS moh,
                 MTL_TXN_REQUEST_LINES mol
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME = 'Move order'
                 AND mmt.move_order_line_id = mol.line_id
                 AND mol.header_id = moh.header_id
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 pha.segment1,           --|| '-' || NVL (pra.release_num, 0),
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 po_headers_all pha,
                 po_releases_all pra
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND rct.po_header_id = pha.PO_HEADER_ID(+)
                 AND pha.PO_HEADER_ID = pra.po_header_id(+)
                 AND rct.PO_RELEASE_ID = pra.PO_RELEASE_ID(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME = 'Purchase order'
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 we.wip_entity_name,
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 mtl_parameters org,
                 WIP_ENTITIES we
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND mmt.organization_id = org.organization_id
                 AND org.PROCESS_ENABLED_FLAG = 'N'
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME = 'Job or Schedule'
                 AND mmt.transaction_source_id = we.wip_entity_id
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 gbh.batch_no,
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 mtl_parameters org,
                 GME_BATCH_HEADER gbh
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND mmt.organization_id = org.organization_id
                 AND org.PROCESS_ENABLED_FLAG = 'Y'
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME = 'Job or Schedule'
                 AND mmt.transaction_source_id = gbh.batch_id
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 TO_CHAR (oeh.order_number),
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 oe_order_headers_all oeh,
                 oe_order_lines_all oel
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME IN ('Sales order')
                 AND mmt.trx_source_line_id = oel.line_id
                 AND oeh.header_id = oel.header_id
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 TO_CHAR (oeh.order_number),
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 oe_order_headers_all oeh,
                 oe_order_lines_all oel
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME IN ('RMA')
                 AND mmt.trx_source_line_id = oel.line_id
                 AND oeh.header_id = oel.header_id
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 TO_CHAR (oeh.order_number),
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 oe_order_headers_all oeh,
                 oe_order_lines_all oel
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME = 'Internal order'
                 AND mmt.trx_source_line_id = oel.line_id
                 AND oeh.header_id = oel.header_id
          UNION ALL
          SELECT mmt.transaction_id,
                 mmt.transaction_type_id,
                 mtt.transaction_type_name,
                 mmt.TRANSACTION_REFERENCE,
                 mtr.REASON_NAME,
                 mmt.SHIPMENT_NUMBER,
                 rsh.receipt_num,
                 TO_CHAR (prha.segment1),
                 mmt.attribute1,
                 mmt.attribute2,
                 mmt.attribute3,
                 mmt.attribute4,
                 mmt.attribute5,
                 mmt.attribute6,
                 mmt.attribute7,
                 mmt.attribute8,
                 mmt.ATTRIBUTE9,
                 mmt.ATTRIBUTE10,
                 mmt.ATTRIBUTE11,
                 mmt.attribute12,
                 mmt.attribute13,
                 mmt.attribute14,
                 mmt.attribute15
            FROM mtl_material_transactions mmt,
                 MTL_TRANSACTION_TYPES mtt,
                 MTL_TRANSACTION_REASONS mtr,
                 rcv_transactions rct,
                 rcv_shipment_headers rsh,
                 MTL_TXN_SOURCE_TYPES tst,
                 PO_REQUISITION_HEADERS_ALL prha
           WHERE     mmt.transaction_type_id = mtt.transaction_type_id
                 AND mtt.transaction_source_type_id =
                        mmt.transaction_source_type_id
                 AND mmt.TRANSACTION_SOURCE_TYPE_ID =
                        tst.TRANSACTION_SOURCE_TYPE_ID
                 AND mmt.REASON_ID = mtr.reason_id(+)
                 AND mmt.RCV_TRANSACTION_ID = rct.TRANSACTION_ID(+)
                 AND rct.SHIPMENT_HEADER_ID = rsh.SHIPMENT_HEADER_ID(+)
                 AND tst.TRANSACTION_SOURCE_TYPE_NAME = 'Internal requisition'
                 AND mmt.transaction_source_id = prha.REQUISITION_HEADER_ID
          ORDER BY 1) ximti,
         HR_LOCATIONS_ALL HLA
   -- END ADD
   WHERE     MSI.INVENTORY_ITEM_ID(+) = MT.INVENTORY_ITEM_ID
         AND MSI.ORGANIZATION_ID(+) = MT.ORGANIZATION_ID
         --
         AND mt.transaction_id = ximti.transaction_id
         --
         --REPLACE BY FARID 7/12/2016
         --AND MT.TRANSFER_LOCATOR_ID = MIL.INVENTORY_LOCATION_ID(+)
         AND MT.LOCATOR_ID = MIL.INVENTORY_LOCATION_ID(+)
         AND MT.TRANSFER_LOCATOR_ID = MIL2.INVENTORY_LOCATION_ID(+)
         --AND MT.ORGANIZATION_ID = MP.ORGANIZATION_ID
         AND WLPN.ORGANIZATION_ID(+) = MT.ORGANIZATION_ID
         --AND MT.LOT_NUMBER = MLN.LOT_NUMBER(+)
         AND MSI.ORGANIZATION_ID = MTLN.ORGANIZATION_ID(+)
         AND MT.LPN_ID = WLPN.LPN_ID(+)
         AND MSI.INVENTORY_ITEM_ID = MTLN.INVENTORY_ITEM_ID(+)
         AND MT.TRANSACTION_ID = MTLN.TRANSACTION_ID(+)
         -- AND MLN.LOT_NUMBER = MTLN.LOT_NUMBER
         AND MT.TRANSACTION_TYPE_ID = MTT.TRANSACTION_TYPE_ID(+)
         AND MTLN.STATUS_ID = MMS.STATUS_ID(+)
         AND MT.TRANSACTION_SOURCE_TYPE_ID = MTSS.TRANSACTION_SOURCE_TYPE_ID(+)
         AND MT.REASON_ID = MTR.REASON_ID(+)
         AND MT.RCV_TRANSACTION_ID = RT.TRANSACTION_ID(+)
         AND NVL (RT.SHIPMENT_HEADER_ID, -1) = RSH.SHIPMENT_HEADER_ID(+)
         AND USR.USER_ID(+) = MTLN.LAST_UPDATED_BY
         AND MT.TRANSACTION_SOURCE_ID = POH.PO_HEADER_ID(+)
         AND MT.TRANSACTION_SOURCE_ID = MSO.SALES_ORDER_ID(+)
         AND mt.transaction_source_id = mtrh.header_id(+)
         AND MT.SHIP_TO_LOCATION_ID = HLA.LOCATION_ID(+)
         AND MT.CONTENT_LPN_ID = WLPN2.LPN_ID(+)
         AND MP.wms_enabled_flag = 'Y'
         AND MT.TRANSACTION_ACTION_ID != 24
ORDER BY MT.SUBINVENTORY_CODE,
         MT.TRANSACTION_DATE,
         MIL.SEGMENT1,
         MSI.SEGMENT1,
         WLPN.LICENSE_PLATE_NUMBER,
         MTLN.LOT_NUMBER

-- Item Category
SELECT mic.organization_id,
       (SELECT organization_code
          FROM mtl_parameters
         WHERE organization_id = mic.organization_id)
          organization_code,
       msi.inventory_item_id,
       msi.segment1 item_code,
       micb.segment1 category,
       mct.description,
       micb.structure_id,
       micb.segment1,
       (SELECT b.description
          FROM fnd_id_flex_segments a, fnd_flex_values_vl b
         WHERE     a.id_flex_code = 'MCAT'
               AND a.id_flex_num = micb.structure_id
               AND a.flex_value_set_id = b.flex_value_set_id
               AND b.flex_value = micb.segment1
               AND a.APPLICATION_COLUMN_NAME = UPPER ('Segment1'))
          segment1_desc,
          micb.segment2,
       (SELECT b.description
          FROM fnd_id_flex_segments a, fnd_flex_values_vl b
         WHERE     a.id_flex_code = 'MCAT'
               AND a.id_flex_num = micb.structure_id
               AND a.flex_value_set_id = b.flex_value_set_id
               AND b.flex_value = micb.segment2
               AND (B.PARENT_FLEX_VALUE_LOW = micb.segment1 or B.PARENT_FLEX_VALUE_LOW is null) 
               AND a.APPLICATION_COLUMN_NAME = UPPER ('Segment2'))
          segment2_desc,
          micb.segment3,
       (SELECT b.description
          FROM fnd_id_flex_segments a, fnd_flex_values_vl b
         WHERE     a.id_flex_code = 'MCAT'
               AND a.id_flex_num = micb.structure_id
               AND a.flex_value_set_id = b.flex_value_set_id
               AND b.flex_value = micb.segment3
                AND (B.PARENT_FLEX_VALUE_LOW = micb.segment2 or B.PARENT_FLEX_VALUE_LOW is null) 
               AND a.APPLICATION_COLUMN_NAME = UPPER ('Segment3'))
          segment3_desc,
          micb.segment4,
          (SELECT b.description
          FROM fnd_id_flex_segments a, fnd_flex_values_vl b
         WHERE     a.id_flex_code = 'MCAT'
               AND a.id_flex_num = micb.structure_id
               AND a.flex_value_set_id = b.flex_value_set_id
               AND b.flex_value = micb.segment4
                AND (B.PARENT_FLEX_VALUE_LOW = micb.segment3 or B.PARENT_FLEX_VALUE_LOW is null) 
               AND a.APPLICATION_COLUMN_NAME = UPPER ('Segment4'))
          segment4_desc,
           micb.segment5,
          (SELECT b.description
          FROM fnd_id_flex_segments a, fnd_flex_values_vl b
         WHERE     a.id_flex_code = 'MCAT'
               AND a.id_flex_num = micb.structure_id
               AND a.flex_value_set_id = b.flex_value_set_id
               AND b.flex_value = micb.segment5
                AND (B.PARENT_FLEX_VALUE_LOW = micb.segment4 or B.PARENT_FLEX_VALUE_LOW is null) 
               AND a.APPLICATION_COLUMN_NAME = UPPER ('Segment5'))
          segment5_desc
  FROM mtl_item_categories mic,
       mtl_system_items msi,
       mtl_categories_b_kfv micb,
       MTL_CATEGORIES_TL mct
 WHERE     mic.inventory_item_id = msi.inventory_item_id
       AND msi.organization_id = mic.organization_id
       AND mic.category_id = micb.category_id
       AND mic.category_id = mct.category_id
       AND mct.language = USERENV ('LANG')



-- Item MASTER
SELECT msi.organization_id,
       mp.organization_code,
       msi.inventory_item_id,
       msi.segment1,
       msi.description,
       msi.primary_uom_code,
       msi.primary_unit_of_measure,
       msi.inventory_item_flag inventory_item,
       msi.lot_control_code,
       msi.AUTO_LOT_ALPHA_PREFIX start_prefix_lot,
       msi.START_AUTO_LOT_NUMBER,
       msi.serial_number_control_code,
       msi.AUTO_SERIAL_ALPHA_PREFIX starting_prefix,
       msi.START_AUTO_SERIAL_NUMBER start_number,
       msi.PURCHASING_ITEM_FLAG,
       msi.PURCHASING_ENABLED_FLAG,
       msi.EXPENSE_ACCOUNT expense_account_id,
       (SELECT CONCATENATED_SEGMENTS
          FROM gl_code_combinations_kfv
         WHERE code_combination_id = msi.EXPENSE_ACCOUNT)
          expense_account_seg,
       msi.ENCUMBRANCE_ACCOUNT,
       (SELECT CONCATENATED_SEGMENTS
          FROM gl_code_combinations_kfv
         WHERE code_combination_id = msi.ENCUMBRANCE_ACCOUNT)
          ENCUMBRANCE_account_seg,
       msi.COSTING_ENABLED_FLAG,
       msi.COST_OF_SALES_ACCOUNT,
       (SELECT CONCATENATED_SEGMENTS
          FROM gl_code_combinations_kfv
         WHERE code_combination_id = msi.COST_OF_SALES_ACCOUNT)
          cost_account_seg,
          msi.INVOICEABLE_ITEM_FLAG,
          msi.INVOICE_ENABLED_FLAG,
       msi.SALES_ACCOUNT,
       (SELECT CONCATENATED_SEGMENTS
          FROM gl_code_combinations_kfv
         WHERE code_combination_id = msi.SALES_ACCOUNT)
          sales_account_seg,
          msi.PRICE_TOLERANCE_PERCENT,
          msi.LIST_PRICE_PER_UNIT,
          msi.UN_NUMBER_ID,
          (select UN_number from PO_UN_NUMBERS_TL where un_number_id =msi.UN_NUMBER_ID and language = USERENV ('LANG'))UN_number
  FROM mtl_system_items msi, mtl_parameters mp
 WHERE     msi.organization_id = mp.organization_id
       AND msi.segment1 = 'BY01'
       AND msi.organization_id = 160;
       
 
		 
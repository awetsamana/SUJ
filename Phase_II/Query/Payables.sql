query GL-SLA-Ap
 SELECT  
           aia.INVOICE_ID "Invoice_Id", 
           AIA.DOC_SEQUENCE_VALUE,
           aia.INVOICE_NUM ,
           aia.attribute6 "INVOICE_ID",
           aia.GL_DATE,
           aia.INVOICE_AMOUNT,
           xal.ACCOUNTED_DR "Accounted DR IN SLA",
           xal.ACCOUNTED_CR "Accounted CR IN SLA",
           gjl.ACCOUNTED_CR "ACCOUNTED_CR IN GL",
           gjl.ACCOUNTED_DR "Accounted DR IN GL",         
           xev.event_type_code,
              gcc.SEGMENT1
           || '.'
           || gcc.SEGMENT2
           || '.'
           || gcc.SEGMENT3
           || '.'
           || gcc.SEGMENT4
           || '.'
           || gcc.SEGMENT5
           || '.'
           || gcc.SEGMENT6
           || '.'
           || gcc.SEGMENT7
              "CODE_COMBINATION",
           aia.GL_DATE,
           xah.PERIOD_NAME,
           aia.VENDOR_ID "Vendor Id",
           aps.VENDOR_NAME "Vendor Name",
           xah.JE_CATEGORY_NAME "JE Category Name",
                      GJH.JE_SOURCE
    FROM   ap_invoices_all aia,
           xla.xla_transaction_entities XTE,
           xla_events xev,
           xla_ae_headers XAH,
           xla_ae_lines XAL,
           GL_IMPORT_REFERENCES gir,
           gl_je_headers gjh,
           gl_je_lines gjl,
           gl_code_combinations gcc,
           ap_suppliers aps
   WHERE       aia.INVOICE_ID = xte.source_id_int_1
           and aia.ACCTS_PAY_CODE_COMBINATION_ID = gcc.code_combination_id
           AND xev.entity_id = xte.entity_id
           AND xah.entity_id = xte.entity_id
           AND xah.event_id = xev.event_id
           AND XAH.ae_header_id = XAL.ae_header_id
              and XAH.je_category_name = 'Purchase Invoices'
           AND GJH.JE_SOURCE = 'Payables'
           AND XAL.GL_SL_LINK_ID = gir.GL_SL_LINK_ID
           and gir.GL_SL_LINK_ID = gjl.GL_SL_LINK_ID
           AND gir.GL_SL_LINK_TABLE = xal.GL_SL_LINK_TABLE
           AND gjl.JE_HEADER_ID = gjh.JE_HEADER_ID
           AND gjl.ledger_id = gjh.ledger_id
           and xah.ledger_id = gjh.ledger_id
           AND gjh.JE_HEADER_ID = gir.JE_HEADER_ID
           and aia.set_of_books_id = gjh.ledger_id
           AND gjl.JE_HEADER_ID = gir.JE_HEADER_ID
           AND gir.JE_LINE_NUM = gjl.JE_LINE_NUM
           AND gcc.CODE_COMBINATION_ID = XAL.CODE_COMBINATION_ID
           AND gcc.CODE_COMBINATION_ID = gjl.CODE_COMBINATION_ID
           AND aia.VENDOR_ID = aps.VENDOR_ID
           --AND gjh.PERIOD_NAME BETWEEN NVL (:PERIOD_FROM, gjh.PERIOD_NAME)
           --                        AND  NVL (:PERIOD_TO, gjh.PERIOD_NAME)               
           --AND gcc.SEGMENT1 = NVL (:seg1, gcc.SEGMENT1)
           --AND gcc.SEGMENT3 = NVL (:seg, gcc.SEGMENT3)

ORDER BY   1, aia.GL_DATE

-- invoice
SELECT aia.invoice_num,
       TO_CHAR (aia.invoice_date, 'DD-MON-RRRR') invoice_date,
       pov.vendor_name,
       asup.segment1 supplier_num,
       --  aia.*,
       assa.vendor_site_code,
       aila.invoice_id,
       aia.invoice_currency_code,
       aia.invoice_amount,
       aila.line_number,
        pla.item_id,
       (SELECT segment1
          FROM mtl_system_items
         WHERE inventory_item_id = pla.item_id AND ROWNUM = 1)
          item_code,
          pla.item_description,
       aila.amount,
       aila.line_type_lookup_code,
       aila.amount,
       aila.po_header_id,
       aila.po_line_id
  FROM ap_invoices_all aia,
       po_vendors pov,
       ap_invoice_lines_all aila,
       ap_supplier_sites_all assa,
       ap_suppliers asup,
       po_lines_all pla
 WHERE     pov.vendor_id = aia.vendor_id
       AND aia.invoice_id = aila.invoice_id
       AND aia.vendor_site_id = assa.vendor_site_id
       --and aia.vendor_id = asa.vendor_id
       AND pov.vendor_id = assa.vendor_id
       AND aia.vendor_id = asup.vendor_id
       AND assa.vendor_id = asup.vendor_id
       AND aila.po_line_id = pla.po_line_id
       AND aia.invoice_num = 'PB09191';
	   
	  
--query payments
SELECT b.segment1 vendor_number,
       b.vendor_name vendor_name,
       c.vendor_site_code,
       c.pay_group_lookup_code,
       a.invoice_num invoice_number,
       a.invoice_date,
       a.gl_date,
       d.due_date,
       a.invoice_currency_code,
       a.invoice_amount,
       a.amount_paid,
       a.pay_group_lookup_code,
       d.payment_priority,
       (SELECT MAX (check_date)
          FROM ap_checks_all aca, ap_invoice_payments_all aip
         WHERE aca.CHECK_ID = aip.CHECK_ID AND aip.invoice_id = a.invoice_id)
          "Last Payment Made on",
          a.cancelled_date
  FROM apps.ap_invoices_all a,
       apps.ap_suppliers b,
       apps.ap_supplier_sites_                                ents_all ap,
       ap_checks_all ac
WHERE     a.vendor_id = b.vendor_id
       AND a.vendor_site_id = c.vendor_site_id
       AND b.vendor_id = c.vendor_id
       AND a.invoice_id = d.invoice_id
       AND ap.invoice_id = a.invoice_id
       AND ac.CHECK_ID = ap.CHECK_ID
       and ac.STATUS_LOOKUP_CODE <> 'VOIDED'
       AND a.org_id = 89
       and a.invoice_id= 1234
       AND a.pay_group_lookup_code IN ('DISTRIBUTOR')
       --AND ac.check_date BETWEEN TO_DATE ('01-Apr-2014', 'DD-MON-YYYY') AND TO_DATE ('30-Jun-2014 23:59:59', 'DD-MON-YYYY HH24:MI:SS')	

--master vendor
/* Formatted on 8/3/2018 1:59:37 PM (QP5 v5.256.13226.35538) */
SELECT aps.vendor_id,
       aps.vendor_name,
       aps.vendor_type_lookup_code,
       apss.org_id,
       hou.name operating_unit,
       aps.creation_date
  FROM ap_suppliers aps, ap_supplier_sites_all apss,hr_operating_units hou
 WHERE aps.vendor_id = apss.vendor_id
 and apss.org_id = hou.organization_id

-- 


--VAT IN 

  SELECT aia.invoice_num,
         aia.invoice_id,
         asup.vendor_name supplier_name,
         asup.segment1 supplier_num,
         PVS.VENDOR_SITE_CODE,
         aia.invoice_type_lookup_code,
         aila.line_number,
         aila.line_source,
         aia.invoice_currency_code,
         aia.invoice_amount,
         aia.invoice_date,
         aia.invoice_received_date,
         AP_INVOICES_PKG.GET_POSTING_STATUS (AIa.INVOICE_ID) POSTING_FLAG,
         AP_INVOICES_PKG.GET_APPROVAL_STATUS (AIa.INVOICE_ID,
                                              AIa.INVOICE_AMOUNT,
                                              AIa.PAYMENT_STATUS_FLAG,
                                              AIa.INVOICE_TYPE_LOOKUP_CODE)
            APPROVAL_STATUS_LOOKUP_CODE,
         AP_INVOICES_PKG.GET_PO_NUMBER (AIa.INVOICE_ID) PO_NUMBER,
         --       (SELECT SUM (amount)
         --          FROM ap_invoice_lines_all
         --         WHERE invoice_id = aia.invoice_id AND LINE_TYPE_LOOKUP_CODE = 'TAX')
         SUM (aila.amount) vat_amount,
         aia.gl_date
    FROM ap_invoices_all aia,
         ap_invoice_lines_all aila,
         ap_suppliers asup,
         PO_VENDOR_SITES_ALL pvs
   WHERE     1 = 1
         AND aia.vendor_id = asup.vendor_id
         AND aia.invoice_id = aila.invoice_id
         AND aia.vendor_site_id = pvs.vendor_site_id
         AND aila.LINE_TYPE_LOOKUP_CODE = 'TAX'
GROUP BY aia.invoice_num,
         aia.invoice_id,
         asup.vendor_name,
         asup.segment1,
         PVS.VENDOR_SITE_CODE,
         aia.invoice_type_lookup_code,
         aia.invoice_currency_code,
         aia.invoice_amount,
         aia.invoice_date,
         aia.invoice_received_date,
         AIa.PAYMENT_STATUS_FLAG,
         AIa.INVOICE_TYPE_LOOKUP_CODE,
         aia.gl_date,
         aila.line_number,
         aila.line_source

	-- WHT 
  SELECT aia.invoice_num,
         aia.invoice_id,
         asup.vendor_name supplier_name,
         asup.segment1 supplier_num,
         PVS.VENDOR_SITE_CODE,
         aia.invoice_type_lookup_code,
         aila.line_number,
         aila.line_source,
         aia.invoice_currency_code,
         aia.invoice_amount,
         aia.invoice_date,
         aia.invoice_received_date,
         AP_INVOICES_PKG.GET_POSTING_STATUS (AIa.INVOICE_ID) POSTING_FLAG,
         AP_INVOICES_PKG.GET_APPROVAL_STATUS (AIa.INVOICE_ID,
                                              AIa.INVOICE_AMOUNT,
                                              AIa.PAYMENT_STATUS_FLAG,
                                              AIa.INVOICE_TYPE_LOOKUP_CODE)
            APPROVAL_STATUS_LOOKUP_CODE,
         AP_INVOICES_PKG.GET_PO_NUMBER (AIa.INVOICE_ID) PO_NUMBER,
         --       (SELECT SUM (amount)
         --          FROM ap_invoice_lines_all
         --         WHERE invoice_id = aia.invoice_id AND LINE_TYPE_LOOKUP_CODE = 'TAX')
         SUM (aila.amount) wht_amount,
         awg.name wht_name,
         awg.description wht_desc,
         aia.gl_date
    FROM ap_invoices_all aia,
         ap_invoice_lines_all aila,
         ap_suppliers asup,
         PO_VENDOR_SITES_ALL pvs,
         ap_awt_groups awg
   WHERE     1 = 1
         AND aia.vendor_id = asup.vendor_id
         AND aia.invoice_id = aila.invoice_id
         AND aia.vendor_site_id = pvs.vendor_site_id
         and aila.awt_group_id = awg.group_id
         --and aia.invoi	ce_num = 'TJB003'
         --AND aila.LINE_TYPE_LOOKUP_CODE = 'TAX'
GROUP BY aia.invoice_num,
         aia.invoice_id,
         asup.vendor_name,
         asup.segment1,
         PVS.VENDOR_SITE_CODE,
         aia.invoice_type_lookup_code,
         aia.invoice_currency_code,
         aia.invoice_amount,
         aia.invoice_date,
         aia.invoice_received_date,
         AIa.PAYMENT_STATUS_FLAG,
         AIa.INVOICE_TYPE_LOOKUP_CODE,
         aia.gl_date,
         aila.line_number,
         aila.line_source,
         awg.name,
         awg.description

	
		 